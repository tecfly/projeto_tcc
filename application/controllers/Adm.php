<?php

class Adm extends MY_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->model('ElisomModel', 'model');
        $this->load->model('CadastroModel', 'model2');
        $this->load->model('EntradaModel', 'model3');
    }

	public function index(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('EntradaModel', 'model3');
            $data['produtos'] = $this->model3->quantprod();
            $data['categorias'] = $this->model3->quantcateg();
            $data['calendario'] = $this->model3->quantcalendario();
            $data['mensagens'] = $this->model3->quantmens();
            $html .= $this->load->view('adm/adm.php', $data, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function lista(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('ElisomModel', 'model');
            $data['tabela'] = $this->model->gera_tabela();
            $html .= $this->load->view('adm/table_lista', $data, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }

    }

    public function editar($id){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('ElisomModel', 'model');
            $data['user'] = $this->model->read($id);
            $html .= $this->load->view('contato/contato_form', $data, true);
            $this->model->edita_usuario($id);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function deletar($id){
        if($this->session->userdata('email') != ''){
            $this->load->model('ElisomModel', 'model');
            $this->model->deletar($id);
            redirect('adm/lista');
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function usuario(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('CadastroModel', 'model2');
            $this->model2->guarda_usuario();
            $html .= $this->load->view('adm/form_funcionario', null, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function lista_usuario(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('CadastroModel', 'model2');
            $data['tabela'] = $this->model2->gera_tabela();
            $html .= $this->load->view('adm/table_lista', $data, true);
            $this->show($html);
        }
    
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function editar_usuario($id){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('CadastroModel', 'model2');
            $data['user'] = $this->model2->read($id);
            $html .= $this->load->view('adm/form_funcionario', $data, true);
            $this->model2->edita_usuario($id);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function deletar_usuario($id){
        if($this->session->userdata('email') != ''){
            $this->load->model('CadastroModel', 'model2');
            $this->model2->deletar($id);
            redirect('adm/lista_usuario');
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function login(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/modal', null, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function controle(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $html .= $this->load->view('estoque/controle', null, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }	
}