<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('CategoriaModel', 'model');
    }

    public function index(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm',null, true);
            $this->load->model('CategoriaModel', 'model');
            $this->model->cria_categoria();
            $html .= $this->load->view('adm/categoria/cadastra_categoria', null, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function lista(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('CategoriaModel', 'model');
            $data['tabela'] = $this->model->gera_table();
            $html .= $this->load->view('adm/categoria/lista_categoria', $data, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function editar($id){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('CategoriaModel', 'model');
            $data['edit'] = $this->model->read($id);
            $html .= $this->load->view('adm/categoria/edita_categoria', $data, true);
            $this->model->edita_categoria($id);
            $this->show($html); 
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function deletar($id){
        if($this->session->userdata('email') != ''){
            $this->load->model('CategoriaModel', 'model');
            $this->model->deletar($id);
            redirect('categoria/lista');
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }
}
