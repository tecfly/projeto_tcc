<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logar extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Login_model', 'model');
        $this->load->helper('form');
    }

    public function login(){
        $this->model->login_validation();
        $html = $this->load->view("adm/login", null, true);
        $this->show($html);

    }
    
    public function logout(){
        $this->session->sess_destroy();
        redirect('principal');
        
    }

 
    public function Email() {
        $this->load->library("email");
        $this->email->set_mailtype('html');
	
    	$email = $this->input->post('email');
        $result = $this->db->query("select * from usuario where email='".$email."'")->result_array();
        $tokan = rand(100000,999999);
            
        $this->db->query("update usuario set senha='".$tokan."' where email='".$email."'" );
        $message = "Por favor, clique no link de redefinir senha: <br> <a href='".base_url('logar/reset?tokan=').$tokan."'> Redefinir senha</a>";

        $this->email
        ->from('elisomautopecas@gmail.com')
        ->to($email)
        ->subject('Link para redefinição de senha')
        ->message('$message');

        if ($this->email->send()){
            echo "enviado!<br>";
            echo $message;
        } else {
            echo $this->email->print_debugger();
            return false;
            
        }
    }

    public function forgotpassword(){
        $this->load->view('adm/forgotpassword');
    }

    public function resetlink(){
        $email = $this->input->post('email');
        $result = $this->db->query("select * from usuario where email='".$email."'")->result_array();
        if (count ($result) > 0){
            $tokan = rand(100000,999999);
            $this->db->query("update usuario set senha='".$tokan."' where email='".$email."'" );
            $message = "Por favor, clique no link de redefinir senha: <br> <a href='".base_url('logar/reset?tokan=').$tokan."'> Redefinir senha</a>";
            $this->Email($email, 'Redefinir senha', $message );
        }
        else {
            $data['invalido']= "<center>Email não registrado no sistema.</center>";
            $this->load->view("adm/forgotpassword", $data);
        }
    }

    public function reset(){
        $data ['tokan'] = $this->input->get('tokan');
        $_SESSION['tokan']= $data ['tokan'];
        $this->load->view("adm/resetpass");
    }

    public function updatepass(){
        $_SESSION['tokan']; 
        $data = $this->input->post();
        if($data['senha']==$data['csenha']){
            $this->db->query("update usuario set senha='".$data['senha']."' where senha='". $_SESSION['tokan']."'");
        }
    }
}