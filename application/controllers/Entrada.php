<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entrada extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('EntradaModel', 'model');
    }

    public function index($value=null){
        if($this->session->userdata('email') != ''){
            if($value==null) {
                $value = 0;
            }
            $reg_p_pag = 10;
            
            if($value >= $reg_p_pag) {
                $data['btnA'] = '';
            }else {
                $data['btnA'] = 'pointer-events: none';
            }
    
            $this->load->model('EntradaModel', 'model');
            $u= $this->model->get_qtd_produtos();
    
            if(($u[0]->total - $value) < $reg_p_pag) {
                $data['btnP'] = 'pointer-events: none';
            }else {
                $data['btnP'] = '';
            }

            $data['value'] = $value;
            $data['reg_p_pag'] = $reg_p_pag;
            $data['qtd_reg'] = $u[0]->total;

            $v_inteiro = (int) $u[0]->total/$reg_p_pag;
            $v_resto = $u[0]->total%$reg_p_pag;

            if($v_resto>0){
                $v_inteiro +=1;
            }

            $data['qtd_botoes'] = $v_inteiro;
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('EntradaModel', 'model');
            $data['tabela'] = $this->model->gera_table($value, $reg_p_pag);
            $html .= $this->load->view('adm/estoque/entrada_lista', $data, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function adicionar($id){
        if($this->session->userdata('email') != ''){
            if(is_null($this->input->post('count_quantidade'))){
                $html = $this->load->view('adm/base/navbaradm', null, true);
                $this->load->model('EntradaModel', 'model');
                $dados['produto'] = $this->model->dados_produto($id)[0];
                $this->load->view('adm/estoque/adicionar', $dados);
                $this->show($html);
            } else {
                $this->load->model('EntradaModel', 'model');
                $this->model->alterar_quantidade($id, $this->input->post('count_quantidade'));
                redirect(base_url('index.php/entrada/index'));
            }
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function subtrair($id){
        if($this->session->userdata('email') != ''){
            if(is_null($this->input->post('count_quantidade'))){
                $html = $this->load->view('adm/base/navbaradm', null, true);
                $this->load->model('EntradaModel', 'model');
                $dados['produto'] = $this->model->dados_produto($id)[0];
                $this->load->view('adm/estoque/subtrair', $dados);
                $this->show($html);
            } 
            else {
                $this->load->model('EntradaModel', 'model');
                $this->model->alterar_quantidade($id, -$this->input->post('count_quantidade'));
                redirect(base_url('index.php/entrada/index'));
            }
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function movimento(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('EntradaModel', 'entrada');
            $data['entrada'] = $this->entrada->entrada();
            $data['saida'] = $this->entrada->saida();
            $html .= $this->load->view('adm/estoque/lista_movimentos', $data, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

}
