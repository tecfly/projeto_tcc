<?php

class Principal extends MY_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('ProdutosModel',  'model');
        $this->load->model('ElisomModel',  'model3');
        $this->load->model('CategoriaModel', 'model4');
        $this->load->model('ProdutosAdmModel', 'model5');
    }

    public function index(){
        $html = $this->load->view('base/navbar', null, true);
        $html .= $this->load->view('adm/modal', null, true);
        $html .= $this->load->view('home/video', null, true);
        $html .= $this->load->view('home/destaque', null, true);
        $this->load->model('ProdutosAdmModel', 'model5');
        $data['promocao'] = $this->model5->promocao();
        $html .= $this->load->view('home/cards', $data, true);
        $html .= $this->load->view('base/rodape', null, true);
        $this->show($html);
    }

    public function sobre(){
        $html = $this->load->view('base/navbar', null, true);
        $html .= $this->load->view('adm/modal', null, true);
        $html .= $this->load->view('sobre/secao', null, true);
        $html .= $this->load->view('sobre/secao2', null, true);
        $html .= $this->load->view('sobre/secao3', null, true);
        $html .= $this->load->view('base/rodape', null, true);
        $this->show($html);
    }

    public function produtos(){
        $html = $this->load->view('base/navbar',null, true);
        $html .= $this->load->view('adm/modal', null, true);
        $this->load->model('CategoriaModel', 'model4');
        $this->load->model('ProdutosModel', 'model');
        $data['categoria_produtos'] = $this->model->lista_categoria();
        $html .= $this->load->view('adm/produtos/lista', $data, true);
        $html .= $this->load->view('base/rodape', null, true);
        $this->show($html);
    }
    
    public function detalhe($id){
        $html = $this->load->view('base/navbar',null, true);
        $html .= $this->load->view('adm/modal', null, true);
        $this->load->model('CategoriaModel', 'model4');
        $data['nome_categoria'] = $this->model4->nome_categoria($id);
        $this->load->model('ProdutosModel', 'model');
        $data['detalhe_produto'] = $this->model->lista_produtos($id);
        $html .= $this->load->view('produtos/detalhe', $data, true);
        $html .= $this->load->view('base/rodape', null, true);
        $this->show($html);
    }

    public function contato(){
        $html = $this->load->view('base/navbar', null, true);
        $html .= $this->load->view('adm/modal', null, true);
        $this->load->model('ElisomModel', 'model3');
        $this->model3->salva_usuario();
        $html .= $this->load->view('contato/contato_form', null, true);
        $html .= $this->load->view('contato/contato', null, true);
        $html .= $this->load->view('contato/botao_wpp', null, true);
        $html .= $this->load->view('base/rodape', null, true);
        $this->show($html);
    }

}
