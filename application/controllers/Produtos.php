<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('ProdutosAdmModel', 'model');
        $this->load->model('CategoriaModel', 'model3');
    }

    public function index(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm',null, true);
            $this->load->model('CategoriaModel', 'model3');
            $data['categoria_produtos'] = $this->model3->lista_categoria_adm();
            $html .= $this->load->view('adm/produtos/lista', $data, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function detalhe($id){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm',null, true);
            $this->load->model('CategoriaModel', 'model3');
            $data['nome_categoria'] = $this->model3->nome_categoria($id);
            $this->load->model('ProdutosAdmModel', 'model');
            $data['detalhe_produto'] = $this->model->lista_produtos_adm($id);
            $html .= $this->load->view('adm/produtos/detalhe', $data, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function cadastroprod(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm',null, true);
            $this->load->model('ProdutosAdmModel', 'model');
            $lista= $this->model->busca_categoria();
            $data = array('categorias' => $lista);
            $html .= $this->load->view('adm/categoria/lista_id_categoria.php', $data, true);
            $this->model->cria_produto();
            $html .= $this->load->view('adm/produtos/cadastra_produto.php', null, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function listacategoria(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm',null, true);
            $this->load->model('ProdutosAdmModel', 'model');
            $lista= $this->model->busca_categoria();
            $dados = array('categorias' => $lista);
            $html .= $this->load->view('adm/categoria/lista_id_categoria.php', $dados, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }


    public function editar_produtos($id){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('ProdutosAdmModel', 'model');
            $lista= $this->model->busca_categoria();
            $dados['saida'] = $this->model->read($id);
	        $lista= $this->model->busca_categoria();
            $data = array('categorias' => $lista);
            $html .= $this->load->view('adm/categoria/lista_id_categoria.php', $data, true);
            $html .= $this->load->view('adm/produtos/cadastra_produto.php', $dados, true);
            $this->model->edita_produtos($id);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function deletar_produtos($id){
        if($this->session->userdata('email') != ''){
            $this->load->model('ProdutosAdmModel', 'model');
            $this->model->deletar($id);
            redirect('produtos/pesquisar');
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function promocao($id=NULL) {
        if($this->session->userdata('email') != ''){
            if($id == NULL) {
                redirect(base_url('index.php/produtos/pesquisar'));
            }			
            $this->load->model('ProdutoAdmModel', 'model');
            $query = $this->model->getProdutoByID($id);
            if($query != NULL) {	
                if ($query->promocao == 1) {
                    $dados['promocao'] = 0;
                } else { 
                    $dados['promocao'] = 1;
                }
                $this->model->promocaoProduto($dados, $query->id);
                redirect(base_url('index.php/produtos/pesquisar'));
            } else {
                redirect(base_url('index.php/produtos/pesquisar'));
            }
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
	}


    public function pesquisar($value=null){
        if($this->session->userdata('email') != ''){
            if($value==null) {
                $value = 0;
            }
            $reg_p_pag = 10;
            
            if($value >= $reg_p_pag) {
                $data['btnA'] = '';
            }else {
                $data['btnA'] = 'pointer-events: none';
            }
    
            $this->load->model('ProdutosAdmModel', 'model');
            $u= $this->model->get_qtd_produtos();
    
            if(($u[0]->total - $value) < $reg_p_pag) {
                $data['btnP'] = 'pointer-events: none';
            }else {
                $data['btnP'] = '';
            }

            $data['value'] = $value;
            $data['reg_p_pag'] = $reg_p_pag;
            $data['qtd_reg'] = $u[0]->total;

            $v_inteiro = (int) $u[0]->total/$reg_p_pag;
            $v_resto = $u[0]->total%$reg_p_pag;

            if($v_resto>0){
                $v_inteiro +=1;
            }

            $data['qtd_botoes'] = $v_inteiro;

            $html = $this->load->view('adm/base/navbaradm', null, true);
            $this->load->model('ProdutosAdmModel', 'model');
            $data['produtos'] = $this->model->gera_table_like($value, $reg_p_pag);
            $html .= $this->load->view('adm/produtos/lista_produto', $data, true);
            $this->show($html);
         }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

}
