<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mycalendario extends MY_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('CalendarioModel', 'model');
    }

	public function index($year = NULL , $month = NULL){
		if($this->session->userdata('email') != ''){
			$html = $this->load->view('adm/base/navbaradm',null, true);
			$this->load->model('CalendarioModel');
			$data['calender'] = $this->CalendarioModel->getcalender($year , $month);
			$html .= $this->load->view('adm/calendario/calendario', $data, true);
			$this->show($html);
		}
		else{
            redirect(base_url('index.php/logar/login'));
        }
	}
	public function cadastra(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm',null, true);
            $this->load->model('CalendarioModel', 'model');
			$this->model->cadastra_calendario();
			$data['tabela'] = $this->model->gera_listacalendario();
            $html .= $this->load->view('adm/calendario/cadastra_calendario.php', $data, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

	public function lista(){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
			$this->load->model('CalendarioModel', 'model');
            $data['tabela'] = $this->model->gera_calendario();
            $html .= $this->load->view('adm/calendario/lista_calendario.php', $data, true);
            $this->show($html);
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function editar($id){
        if($this->session->userdata('email') != ''){
            $html = $this->load->view('adm/base/navbaradm', null, true);
			$this->load->model('CalendarioModel', 'model');
            $data['calendario'] = $this->model->read($id);
            $html .= $this->load->view('adm/calendario/edita_calendario.php', $data, true);
            $this->model->edita_calendar($id);
            $this->show($html); 
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }

    public function deletar($id){
        if($this->session->userdata('email') != ''){
			$this->load->model('CalendarioModel', 'model');
            $this->model->deletar($id);
            redirect('mycalendario/lista');
        }
        else{
            redirect(base_url('index.php/logar/login'));
        }
    }
}
