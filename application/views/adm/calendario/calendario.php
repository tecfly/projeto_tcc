<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" href="<?= base_url('assets/mdb/css/calendar.css')?>">
<div class="col-md-10  mt-5 mb-5 table-responsive" >
	<div id="container">
		<center><h2 style="font-family:Dosis;">Bem vindo ao calendário de eventos!</h2></center>
		<div id="body" class="table-responsive table-sm">
			<center><?php echo $calender; ?></center>
		</div>
	</div>
</div>
