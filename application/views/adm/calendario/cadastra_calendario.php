<div class="container my-4">
<br>
    <div class="mt-2">
        <?php
            echo validation_errors('<div class="alert alert-danger">', '</div>');
        ?>

        <?php 
            if (isset($_GET['cadastrado']) && $_GET['cadastrado']){
                echo "<center class='mt-2 mb-3 text-center'><b>Cadastro realizado com sucesso! 
                <a href='lista'> Acesse aqui para edição/exclusão de assuntos do calendário cadastrado no sistema.</a></b></center>"; 
            }
        ?> 
        <div class="row">         
            <div class="col-md-6">
                <form method="POST" enctype="multipart/form-data" class="text-center  mx-auto">
                    <p class="h4 mb-4" style="font-family:Dosis;"> <i class="far fa-calendar-alt"></i> Cadastrar evento no calendário</p>
                    <div class="form-row">
                        <div class="col-md-4">
                            <input class=" form-control mt-2" type="date" name="date" value="<?= set_value('date')?><?= isset($calendario) ? $calendario['date'] : '' ?>"  placeholder="Data de entrega"> 
                        </div>
                        <div class="col-md-8">
                            <input class=" form-control mt-2" type="text" name="content" value="<?= set_value('content')?><?= isset($calendario) ? $calendario['content'] : '' ?>"  placeholder="Assunto para adicionar no calendário">
                        </div>
                    
                    </div>
                    <div class="form-row mt-4">
                        <div class="col-md-12">
                            <button class="btn btn-info btn-block indigo darken-4"  type="submit">Cadastrar evento no calendário</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
				<div class="border">
					<?= $tabela ?>
				</div>
			</div>
		</div>
	</div>
</div>