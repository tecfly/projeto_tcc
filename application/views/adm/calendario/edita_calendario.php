<div class="container mt-3">
    <?php
        echo validation_errors('<div class="alert alert-danger">', '</div>');
    ?>

    <?php 
        if (isset($_GET['cadastrado']) && $_GET['cadastrado']){
            echo "<center class='mt-2 text-center'><b>Cadastro realizado com sucesso! 
            <a href='lista'> Acesse aqui a lista de assuntos do calendário cadastrado no sistema.</a></b></center>"; 
        }
    ?> 

   <div class="row">         
        <div class="col-md-12 mx-auto">
            <form method="POST" enctype="multipart/form-data" class="text-center  mx-auto">
                <p class="h4 mb-4"> <i class="far fa-calendar-alt"></i> Editar evento no calendário</p>
                <div class="form-row mb-4">
                    <div class="col-md-4">
                        <input class=" form-control" type="date" name="date" value="<?= isset($calendario) ? $calendario['date'] : '' ?>"  placeholder="Data de entrega"> 
                    </div>
                    <div class="col-md-8">
                        <input class=" form-control" type="text" name="content" value="<?= isset($calendario) ? $calendario['content'] : '' ?>"  placeholder="Escreva qual evento">
                    </div>
                   
                </div>
                <div class="form-row mb-4">
                    <div class="col-md-12">
                        <button class="btn btn-info btn-block indigo darken-4 my-4"  type="submit">Cadastrar evento no caléndário</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>