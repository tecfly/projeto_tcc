<nav class="navbar navbar-expand-sm navbar-dark indigo darken-4">
	<button class=" btn btn-link btn-sm text-white" id="sidebarToggle" href="#">
      <i class="fas fa-bars text-white"></i>
    </button>
    <img src="<?= base_url('assets/img/logotipo_oficial.png')?>" width="30px" height="30px" alt="Autopeças Elisom">
	<div class="ml-auto">
		<ul class="navbar-nav ml-auto ml-md-0">
		  <li class="nav-item ml-auto dropdown">
			<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
			  aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i> Administrador(a)
			</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink-333">
			  <a class="dropdown-item" data-toggle="modal" data-target="#logoutModal">Sair  <i class="fas fa-sign-out-alt"></i></a>
			</div>
		  </li>
		</ul>
	</div>
</nav>
  <div id="wrapper" style="font-family:Dosis; font-size:20px;">
    <ul class="sidebar navbar-nav navbar-dark">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('index.php/adm')?>">
          <i class="fas fa-fw  fa-home"></i>
          <span style="font-size:20px;">Home</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('index.php/produtos')?>">
        <i class="fas fa-eye"></i>
          <span> Categorias e produtos</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-table"></i>          
        <span>Produtos</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="<?= base_url('index.php/produtos/cadastroprod')?>">Cadastrar</a>
          <a class="dropdown-item" href="<?= base_url('index.php/produtos/pesquisar')?>">Gerenciar</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-align-justify"></i>
          <span>Categorias</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="<?= base_url('index.php/categoria/index')?>">Cadastrar</a>
          <a class="dropdown-item" href="<?= base_url('index.php/categoria/lista')?>">Gerenciar</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="far fa-address-card"></i>
          <span>Usuários</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="<?= base_url('index.php/adm/usuario')?>">Cadastrar</a>
          <a class="dropdown-item" href="<?= base_url('index.php/adm/lista_usuario')?>">Gerenciar</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-undo-alt"></i>
          <span>Estoque</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="<?= base_url('index.php/entrada')?>">Gerenciar</a>
          <a class="dropdown-item" href="<?= base_url('index.php/entrada/movimento')?>">Entrada/Saída</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="far fa-calendar-alt"></i>
          <span>Calendário</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="<?= base_url('index.php/mycalendario')?>">Visualizar calendário</a>
          <a class="dropdown-item" href="<?= base_url('index.php/mycalendario/cadastra')?>">Cadastrar evento</a>
          <a class="dropdown-item" href="<?= base_url('index.php/mycalendario/lista')?>">Gerenciar</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('index.php/adm/lista')?>">
          <i class="fas fa-fw fa-comments"></i>
          <span>Mensagens</span></a>
      </li>
    </ul>

    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
		  <div class="modal-content">
			<div class="modal-header">
			  <h5 class="modal-title" id="exampleModalLabel">Pronto para partir?</h5>
			  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span>
			  </button>
			</div>
			<div class="modal-body" style="font-family:Dosis;">Selecione "Sair" abaixo se você estiver pronto para encerrar sua sessão atual.</div>
			<div class="modal-footer">
			  <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
			  <a class="btn indigo darken-4 text-white" style="font-family:Dosis;" href="<?= base_url('index.php/logar/logout') ?>"  >Sair</a>
			</div>
		  </div>
		</div>
	</div>