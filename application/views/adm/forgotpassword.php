<link rel="stylesheet" href="<?= base_url('assets/mdb/css/login.css')?>">
<link rel="stylesheet" href="<?= base_url('assets/mdb/css/sb-admin.css')?>">
<body class="">
  <?php echo $this->session->set_flashdata('message'); ?>
  <br>
  <br>
  <?php 
    if (isset($invalido)){
       echo $invalido;
    }
  ?>
  <br><br><br>
      <div class="login-box mt-5">
        <img src="<?= base_url('assets/img/logotipo_oficial.png')?>" class="avatar" alt="Avatar Image">
        <h1> <b>Esqueceu sua senha?</b> </h1>
        <form method="post" action="<?= base_url('index.php/logar/resetlink'); ?>">
          <label for="email">Email</label>
          <input type="text" id="email" name="email" placeholder="Digite o email">
          <input type="submit" value="Enviar link de redefinição">
        </form>
      </div>
</body>