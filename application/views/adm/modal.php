<div class="container">
	<?php
	echo validation_errors('<div class="alert alert-danger text-center">', '</div>');
	?>

    
	<form method="post" action="<?php echo base_url('index.php/logar/login'); ?>">
		<div class="modal fade"  id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		  aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content white">
			  <div class="modal-header text-center">
				<div class="mb-5 login-box">
				  <img src="<?= base_url('assets/img/logotipo_oficial.png')?>" class="avatar" alt="Avatar Image">
				</div>
				<button type="button" class="close black-text" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  
			  <div class="modal-body mx-3">
				<h4 class="modal-title w-100 font-weight-bold text-center black-text"> <br>Login</h4>
				<div class="md-form mb-5 black-text">
				  <i class="fas fa-envelope prefix grey-text"></i>
				  <input type="email" name="email" class="form-control validate black-text">
				  <label data-error="Errado" data-success="Correto" for="defaultForm-email">Email</label>
				</div>

				<div class="md-form mb-4 black-text">
				  <i class="fas fa-lock prefix grey-text"></i>
				  <input type="password" name="senha"  class="form-control validate  black-text">
				  <label data-error="Errado" data-success="Correto" for="defaultForm-pass">Senha</label>
				</div>
			  </div>
			  <div class="modal-footer d-flex justify-content-center">
				<a class="col-md-12 mx-auto black-text"><button class="white col-md-12 mx-auto btn white text-black">Acessar</button></a>
						<?php 
						  if (isset($invalido)){
							  echo $invalido;
						  }
						?>
			  </div>
			</div>
		  </div>
		</div>
	</form>
</div>