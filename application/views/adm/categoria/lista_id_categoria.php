<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="font-family:Dosis;color:#00008B;">  Nº que identifica a categoria: </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div style="font-weight:bold;" class="modal-body text-center">
                <table class="table responsive" style="font-family:Dosis;">
                    <tr>
                        <th><center><strong>Nº</strong></center></th>
                        <th><center><strong>Categoria<strong> </center> </th>
                    </tr>
                    <?php foreach ($categorias as $categoria) : ?>
                    <tr>
                        <td><center> <?= $categoria['id'] ?> </center> </td>
                        <td><center> <?= $categoria['categoria'] ?> </center></td>
                    </tr>
                    <?php endforeach;?>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn indigo darken-4 text-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
                
