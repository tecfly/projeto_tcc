<div class="container mt-5">
    <?php
    echo validation_errors('<div class="alert alert-danger text-center">', '</div>');
    ?>
    <?php 
        if (isset($_GET['cadastrado']) && $_GET['cadastrado']){
            echo "<center class='mt-2'><b>Cadastro realizado com sucesso! 
            <a href='lista'> Acesse aqui a lista de categoria cadastrado no sistema.</a></b></center>"; 
        }
    ?>  
    <h4 class="text-center" style="font-family:Dosis;"> <i class="fas fa-bars"></i> Cadastrar nova categoria </h4>
    <form method="POST" class=" text-center border-light mt-4" enctype="multipart/form-data">
        <div class="col-md-12 mx-auto">
            <input class="col-md-12 mr-4 mb-3 form-control"  type="text" value="<?= set_value('categoria[categoria]')?><?= isset($edit) ? $edit['categoria'] : '' ?>" name="categoria[categoria]"  class="form-control mb-4" placeholder="Qual é a nova categoria?">
            <input class="col-md-12 mb-3"  type="file" value="<?= set_value('foto')?><?= isset($edit) ? $edit['foto'] : '' ?>" name="foto"  class="form-control mb-4" placeholder="Foto da categoria">
            <button class="col-md-12 btn indigo darken-4 text-white btn-block" type="submit"  placeholder="Selecione um arquivo">Cadastrar categoria</button>
        </div>
    </form>
</div>

