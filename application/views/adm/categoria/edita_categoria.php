
<div class="container mt-5 mx-auto">
    <?php
    echo validation_errors('<div class="alert alert-danger">', '</div>');
    ?>

    <h4 class="text-center" style="font-family:Dosis;"> <i class="fas fa-bars"></i> Editar Categoria </h4>
    <form method="POST" class=" text-center border-light mt-4" enctype="multipart/form-data">
        <div class="col-md-12 mx-auto">
            <input class="col-md-12 mr-4 mb-3 form-control"  type="text" value="<?= isset($edit) ? $edit['categoria'] : '' ?>" name="categoria[categoria]"  placeholder="Qual é a nova categoria?">
            <input class="col-md-12 mb-3"  type="file" value="<?= isset($edit) ? $edit['foto'] : '' ?>" name="foto"  placeholder="Foto da categoria">
            <button class="col-md-12 btn indigo darken-4 text-white btn-block" type="submit"  placeholder="Selecione um arquivo">Cadastrar categoria</button>
        </div>
    </form>
</div>