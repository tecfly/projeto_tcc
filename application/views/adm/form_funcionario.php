<div class="fundo mb-4" style=" background-size:cover; width:100%; max-width:100%; background-repeat:no-repeat;">

	<?php
	echo validation_errors('<div class="alert alert-danger">', '</div>');
	?>

    <?php 
        if (isset($_GET['cadastrado']) && $_GET['cadastrado']){
                echo "<center class='mt-2'><b>Cadastro realizado com sucesso! 
                <a href='lista_usuario'>Acesse aqui a lista de usuário cadastrado no sistema.</a></b></center>"; 
        }
    ?>


           <div class="text-center col-md-12 mt-5 mx-auto">
            <form method="POST>
                    <p class="mb-4" style="font-family:Dosis;"><h4><i class="fas fa-angle-double-right"></i> Cadastrar usuário no sistema</h4> </p>
                    <div class="form-row mb-2">
                        <div class="col-md-6 mt-2">
                            <input type="text" value="<?= set_value('nome')?><?= isset($user) ? $user['nome'] : '' ?>" class="form-control white black-text col-xs-12" name="nome" id="nome" placeholder="Digite o primeiro nome">
                        </div>
                        <div class="col-md-6 mt-2">
                            <input type="text" value="<?= set_value('sobrenome')?><?= isset($user) ? $user['sobrenome'] : '' ?>" class="form-control white black-text col-xs-12" name="sobrenome" id="sobrenome" placeholder="Digite o sobrenome">
                        </div>
                    </div>
                    
                    <div class="form-row mb-4">
                        <div class="col-md-6 mt-2">
                            <input type="text" value="<?= set_value('email')?><?= isset($user) ? $user['email'] : '' ?>" class="form-control white black-text col-xs-12" name="email" id="email" placeholder="Digite o email">
                        </div>
                        <div class="col-md-6 mt-2">
                            <input type="password" value="<?= set_value('senha')?><?= isset($user) ? $user['senha'] : '' ?>" class="form-control white black-text col-xs-12" name="senha" id="senha" placeholder="Digite a senha">
                        </div>
                    
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info btn-block indigo darken-4" value="Realizar cadastro">Realizar cadastro</button>
                        </div>
                    </div>
                </form>
              </div>  

</div>