<link rel="stylesheet" href="<?= base_url('assets/mdb/css/login.css')?>">
<link rel="stylesheet" href="<?= base_url('assets/mdb/css/sb-admin.css')?>">

<div class="">
  <br>
  <?php
  echo validation_errors('<div class="alert alert-danger text-center">', '</div>');
  ?>
  <br>
  <br>
  <br>
</div>
<body class="mt-4">
  <br>
      <div class="login-box mt-5">
        <img src="<?= base_url('assets/img/logotipo_oficial.png')?>" class="avatar" alt="Avatar Image">
        <h1> <b>LOGIN</b> </h1>
        <form method="post" action="<?php echo base_url('index.php/logar/login'); ?>">
          <label for="email">Email</label>
          <input type="text" name="email" placeholder="Digite o email">
          <label for="senha">Senha</label>
          <input type="password" name="senha" placeholder="Digite sua senha">
          <input type="submit" value="Entrar">
          <!--<a href="<?= base_url('index.php/logar/forgotpassword')?>">Esqueceu a senha?</a>-->
          <?php 
          if (isset($invalido)){
              echo $invalido;
          }
          ?>
        </form>
      </div>
</body>
