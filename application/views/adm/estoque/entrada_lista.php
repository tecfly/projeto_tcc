<div class="table-responsive">
	<table class="table table-sm border" style="font-family:arial;">
        <thred class="indigo darken-4 white-text text-center">
            <tr class="text-center indigo darken-4 white-text">
                <th scope="col"> Nº </th>
                <th scope="col"> Produto </th>
                <th scope="col"> Categoria </th>
                <th scope="col"> Quantidade </th>
                <th scope="col"> Adicionar </th>
                <th scope="col"> Retirar </th>
            </tr>
        </thred>
        <?php foreach ($tabela as $estoque):?>
        <tr class="text-center">
            <td> <?= $estoque->id ?>  </td>
            <td> <?= $estoque->nome ?> </td>
            <td> <?= $estoque->categ ?> </td>
            <td> <?= $estoque->quantidade ?> </td>
            <td><a href="<?= base_url('index.php/entrada/adicionar/'.($estoque->id).'')?>"><i style="color:green;" class="fas fa-plus-square"></i></a></td>
            <td><a href="<?= base_url('index.php/entrada/subtrair/'.($estoque->id).'')?>"><i style="color:red;" class="fas fa-minus-square"></i></a></td>
        </tr>
        <?php endforeach;?>
    </table>

    <nav>
        <ul class="pagination pg-blue">
            <li class="page-item">
                <a href="<?= base_url('index.php/entrada/index/'.($value - $reg_p_pag)) ?>" class="page-link" aria-label="Anterior" style="<?= $btnA ?>">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Anterior</span>
                </a>
            </li>
            <?php
                $n_pag = 0;
                for ($i=1; $i<=$qtd_botoes; $i++){ ?>
                    <li  class="page-item active info border"><a href="<?= base_url('index.php/entrada/index/'.$n_pag) ?>" class="page-link text-white"><?= $i ?></a></li>
            <?php
                $n_pag+=$reg_p_pag;
                }
            ?>
            <li class="page-item">
                <a href="<?= base_url('index.php/entrada/index/'.($value + $reg_p_pag)) ?>" class="page-link" aria-label="Próximo" style="<?= $btnP ?>">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Próximo</span>
                </a>
            </li>
        </ul>
    </nav>
</div>