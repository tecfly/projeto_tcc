<div class="container mt-2" style="font-family:arial;">
	<h3 class="text-center my-4" style="font-family:dorsis;color:#00008B;">
		Movimentação de entrada/saída do estoque
	</h3>
    <div class="row">
        <div class="col-md-6">
			<h5 class="text-center" style="font-family:dorsis;">Entrada</h5>
            <table class="table table-smtable-responsive border">
                <thred class="indigo darken-4 white-text text-center">
                    <tr class="text-center indigo darken-4 white-text">
                        <th scope="col"> Nome do produto </th>
                        <th scope="col"> Quant. Entrada </th>
                    </tr>
                </thred>
                <?php foreach ($entrada as $entrada):?>
                    <tr class="text-center">
                        <td> <?= $entrada->nome ?> </td>
                        <td> <?= $entrada->quantidade ?> </td> 
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
        <div class="col-md-6">
			<h5 class="text-center" style="font-family:dorsis;">Saída</h5>
            <table class="table table-smtable-responsive border">
                <thred class="indigo darken-4 white-text text-center">
                    <tr class="text-center indigo darken-4 white-text">
                        <th scope="col"> Nome do produto </th>
                        <th scope="col"> Quant. Saída </th>
                    </tr>
                </thred>
                <?php foreach ($saida as $saida):?>
					<tr class="text-center">
						<td> <?= $saida->nome ?> </td>
						<td> <?= $saida->quantidade ?> </td>
					</tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>
</div>