<?php
    defined('BASEPATH') OR exit('URL inválida.');
?>
<div class="container pt-5 pb-5" style="font-family:dorsis;">
    <div class="row">
        <div class="col-sm-8 offset-sm-2 col-12">
            <div class="card p-4">
				<h4 style="color:#00008B;"><?php echo $produto['nome'] ?></h4>
					<p>Quantidade atual: <?php echo $produto['quantidade'] ?></p>
				<hr>
				<form action="<?php echo site_url('entrada/adicionar/'.$produto['id']) ?>" method="post">                
					<div class="form-group">
						<div class="col-sm-2 offset-sm-5 col-4 offset-4">
							<input type="number" name="count_quantidade" class="form-control" value=1 min=1 max=1000>
						</div>
					</div>
					<div class="text-center">
						<a href="<?php echo site_url('entrada/index') ?>" class="btn indigo darken-4 text-white">Cancelar</a>
						<button type="submit" class="btn indigo darken-4 text-white">Adicionar</button>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>