<div class="table-responsive">
    <form action="<?= base_url('index.php/produtos/pesquisar'); ?> " method="post">
        <div class="form-row mb-4 mt-3">
            <div class="col-md-10">
                <input type="text" name="busca" id="busca" placeholder="Pesquisar por..." class="form-control">
            </div>
            <div class="col-md-2">
				<button type="submit" class="btn btn-primary btn-block">Buscar</button>
            </div>
        </div>
    </form>

    <table class="table table-sm col-md-12 border" style="font-family:arial;">
		<thred class="indigo darken-4 white-text text-center">
            <tr class="text-center indigo darken-4 white-text">
                <th scope="col"> Nº </th>
                <th scope="col"> Nome </th>
                <th scope="col"> Categoria </th>
                <th scope="col"> Descrição </th>
                <th scope="col"> Quantidade </th>
                <th scope="col"> Preço </th>
                <th scope="col"> Promoção </th>
                <th scope="col"> Editar </th>
                <th scope="col"> Deletar </th>
            </tr>
        </thred>
        <?php foreach ($produtos as $produto):?>
        <tr class="text-center">
            <td> <?= $produto->id ?>  </td>
            <td> <?= $produto->nome ?> </td>
            <td> <?= $produto->categ ?> </td>
            <td> <?= $produto->descricao ?> </td>
            <td> <?= $produto->quantidade ?> </td>
            <!--<td> <?= $produto->foto ?> </td>-->
            <td> <?= $produto->preco ?> </td>
            <td>
                <?php
                if ($produto->promocao == 1) {
                echo '<span class=""><a href="'.base_url('index.php/produtos/promocao/'.$produto->id.'').'"class="btn  green accent-4
                text-white btn-rounded btn-sm my-0"  title="Deixar inativo">Ativado</a></span>';
                } else {
                echo '<span class="label label-warning"><a href="'.base_url('index.php/produtos/promocao/'.$produto->id.'').'" class="btn  grey lighten-2 btn-rounded btn-sm my-0" title="Deixar Ativo">Inativo</a></span>';
                }
                ?> 
            </td>

            <td><a href="<?= base_url('index.php/produtos/editar_produtos/'.($produto->id).'')?>"><button type="button"
                    class="btn btn-info btn-rounded btn-sm my-0">Editar</button></a></td>
            <td><a href="<?= base_url('index.php/produtos/deletar_produtos/'.($produto->id).'')?>"><button type="button"
                    class="btn btn-danger btn-rounded btn-sm my-0">Excluir</button></a></td>
        </tr>
        <?php endforeach;?>
    </table>

    <nav>
        <ul class="pagination pg-blue">
            <li class="page-item">
                <a href="<?= base_url('index.php/produtos/pesquisar/'.($value - $reg_p_pag)) ?>" class="page-link" aria-label="Anterior" style="<?= $btnA ?>">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Anterior</span>
                </a>
            </li>
            <?php
                $n_pag = 0;
                for ($i=1; $i<=$qtd_botoes; $i++){ ?>
                    <li class="page-item active info border"><a href="<?= base_url('index.php/produtos/pesquisar/'.$n_pag) ?>" class="page-link"><?= $i ?></a></li>
            <?php
                $n_pag+=$reg_p_pag;
                }
            ?>
            <li class="page-item">
                <a href="<?= base_url('index.php/produtos/pesquisar/'.($value + $reg_p_pag)) ?>" class="page-link" aria-label="Próximo" style="<?= $btnP ?>">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Próximo</span>
                </a>
            </li>
        </ul>
    </nav>

    <div id="empty_table_message" class="d-none text-center">
		Nenhum produto relacionado a <strong>"<?= $this->input->post('busca');?>"</strong> foi encontrado. 
		<a href="<?= base_url('index.php/produtos/pesquisar')?>"> Clique aqui para voltar</a>
	</div>

</div>