<div class="container mt-3">
   <div class="row">                                
        <div class="col-md-12 mx-auto">
            <?php
                echo validation_errors('<div class="alert alert-danger text-center">', '</div>');
            ?>

            <?php 
                if (isset($_GET['cadastrap']) && $_GET['cadastrap']){
                    echo "<center class='mt-2 text-center'><b>Cadastro realizado com sucesso! 
                    <a href='pesquisar'> Acesse aqui a lista de produtos cadastrado no sistema.</a></b></center>"; 
                }
            ?> 
        
            <button type="button" class="btn indigo darken-4 white-text mb-3 mr-auto" data-toggle="modal" 
             data-target="#basicExampleModal">Lista o Nº das categorias</button>

            <form method="POST" enctype="multipart/form-data" class="text-center mx-auto">
                <p class="h4 mb-4" style="font-family:Dosis;"><i class="fas fa-barcode"> </i> Cadastrar novo produto </p>
                <div class="form-row mb-2">
                    <div class="col-md-8">
                        <input class=" form-control mt-2" type="text" name="produtos[nome]" value="<?= set_value('produtos[nome]')?><?= isset($saida) ? $saida['nome'] : '' ?>"  placeholder="Nome do Produto"> 
                    </div>
                    <div class="col-md-4">
                        <input class=" form-control mt-2" type="number" name="produtos[categ]" value="<?= set_value('produtos[categ]')?><?= isset($saida) ? $saida['categ'] : '' ?>"  placeholder="Identifique o Nº da Categoria">
                    </div>
                </div>
                
                <div class="form-row mb-2">
                    <div class="col-md-8">
                        <input class=" form-control mt-2" type="text" name="produtos[descricao]" value="<?= set_value('produtos[descricao]')?><?= isset($saida) ? $saida['descricao'] : '' ?>"  placeholder="Descrição do Produto">
                    </div>
                    <div class="col-md-4">
                        <input id="preco" class="preco form-control mt-2" type="text" name="produtos[preco]" value="<?= set_value('produtos[preco]')?><?= isset($saida) ? $saida['preco'] : '' ?>"  placeholder="Preço do Produto">
                    </div>
                   
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-8">
                        <input class=" form-control mt-2" type="file" name="foto" value="<?= set_value('produtos[foto]')?><?= isset($saida) ? $saida['foto'] : '' ?>" >
                    </div>
                    <div class="col-md-4">
                        <input class=" form-control mt-2" type="number" name="produtos[quantidade]" value="<?= set_value('produtos[quantidade]')?><?= isset($saida) ? $saida['quantidade'] : '' ?>"  placeholder="Quantidade em Estoque">
                    </div>
                </div>
                <div class="form-row mb-2">
                    <div class="col-md-12">
                        <button class="btn btn-info btn-block indigo darken-4 my-4" name="cadastrar"  type="submit">Cadastrar produto</button>
                    </div>
        
                </div>
            </form>
        </div>
    </div>
</div>


