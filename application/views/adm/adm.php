<div class="container col-md-8 col-sm-12 col-xs-12">
    <div class=" mx-auto text-center ">
        <center>
            <img class="mt-3 mb-3" src="<?= base_url('assets/img/logotipo_oficial.png')?>" width="100px" height="100px" alt="Autopeças Elisom">
        </center>
        <div class="row">
            <div class="col-md-6 mx-auto mb-4">
                <div class="card p-4" style="font-family:Dorsis;">
                <hr>
                    <h4  style="color:#00008B;font-weight:bold;">Categorias cadastradas no catálogo</h4>
                    <p>Quantidade atual: <?= $categorias ?></p>
                    <a href="<?= base_url('index.php/categoria/lista')?>">Visualizar</a>
                    <hr>
                </div> 
            </div>
            <div class="col-md-6 mx-auto mb-4" style="font-family:Dorsis;">
                <div class="card p-4">
                <hr>
                    <h4 style="color:#00008B;font-weight:bold;">Produtos cadastrados no catálogo</h4>
                    <p>Quantidade atual: <?= $produtos ?></p>
                    <a href="<?= base_url('index.php/produtos/pesquisar')?>">Visualizar</a>
                    <hr>
                </div> 
            </div>
        </div>
        <div class="row mt-4 mb-2">
            <div class="col-md-6 mx-auto mb-4">
                <div class="card p-4" style="font-family:Dorsis;">
                <hr>
                    <h4  style="color:#00008B;font-weight:bold;">Eventos encontrados no caléndário</h4>
                    <p>Quantidade atual: <?= $calendario ?></p>
                    <a href="<?= base_url('index.php/mycalendario/lista')?>">Visualizar</a>
                    <hr>
                </div> 
            </div>
            <div class="col-md-6 mx-auto" style="font-family:Dorsis;">
                <div class="card p-4">
                <hr>
                    <h4  style="color:#00008B;font-weight:bold;">Mensagens</h4>
                    <p>Quantidade atual: <?= $mensagens ?></p>
                    <a href="<?= base_url('index.php/adm/lista')?>">Visualizar</a>
                    <hr>
                </div> 
            </div>
        </div>
    </div>
</div>