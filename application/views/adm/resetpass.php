<link rel="stylesheet" href="<?= base_url('assets/mdb/css/login.css')?>">
<link rel="stylesheet" href="<?= base_url('assets/mdb/css/sb-admin.css')?>">
<body class="mt-4">
  <br>
  <br>
  <?php 
    if (isset($invalido)){
       echo $invalido;
    }
  ?>
  <br><br><br>
      <div class="login-box mt-5">
          <img src="<?= base_url('assets/img/logotipo_oficial.png')?>" class="avatar" alt="Avatar Image">
          <h1> <b>Redefinir senha</b> </h1>
        <form method="post" action="<?= base_url('index.php/logar/updatepass'); ?>">
          <input type="password" id="senha" name="senha" placeholder="Digite a nova senha">
          <input type="password" id="csenha" name="csenha" placeholder="Confirme a senha">
          <input type="submit" value="Redefinir senha">
        </form>
      </div>
</body>