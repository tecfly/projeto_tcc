<div class="fundo mb-4" style="background:url('<?= base_url('assets/img/fundo.jpg')?>'); background-size:cover; width:100%; max-width:100%; background-repeat:no-repeat;">
<br>
<br>
<br>
<br>
	<?php
	echo validation_errors('<div class="alert alert-danger text-center">', '</div>');
	?>
	
	<?php 
        if (isset($_GET['enviado']) && $_GET['enviado']){
            echo "<center class=' mt-2 text-center green-text'><b>Mensagem enviada com sucesso! 
            Agradecemos pela preferência.</b></center>"; 
        }
    ?> 
	<div class="contato mt-5 mb-4">
		<div class="contenador container">
			<form method="POST" class="form col-xs-12 ">
				<div class="">
					<h3 class="title">Fale conosco</h3>
				</div>

				<label for="nome" class="form-label col-xs-12">Nome completo:</label>
				<input type="text" value="<?= set_value('nome')?><?= isset($user) ? $user['nome'] : '' ?>" class="form-input col-xs-12" name="nome" id="nome" placeholder="Digite seu nome completo">

				<label for="email" class="form-label col-xs-12">Email:</label>
				<input type="text" value="<?= set_value('email')?><?= isset($user) ? $user['email'] : '' ?>" class="form-input col-xs-12" name="email" id="email" placeholder="Digite seu email">

				<label for="telefone" class="form-label col-xs-12">Telefone:</label>
				<input type="text" id="telefone" value="<?= set_value('telefone')?><?= isset($user) ? $user['telefone'] : '' ?>" class="telefone form-input col-xs-12" name="telefone" id="telefone" placeholder="Ex: (DDD)000000000 ou (DDD)00000000">
						
				<label for="mensagem" class="form-label col-xs-12">Mensagem:</label>
				<textarea type="text"  class="form-textarea col-xs-12" name="mensagem" id="mensagem" placeholder="Escreva uma mensagem"><?= set_value('mensagem')?><?= isset($user) ? $user['mensagem'] : '' ?></textarea>

				<input type="submit" class="btn-submit morpheus-den-gradient col-xs-12" value="Enviar mensagem">
			</form>
		</div>
	</div>      
	<div class="mt-4 mb-4 container">
		<div class=" col-md-12 col-sm-12 col-xs-12">
			<div class=" card">
			  <div class="card-body">
				<h5 class="card-title"><i class="fas fa-map-marker-alt blue-text"></i> Localização</h5>
				<p class="card-text"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14644.030745823198!2d-46.419304!3d-23.4240895!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x515970b5c27794d3!2sElison%20Auto%20Pe%C3%A7as%20E%20Acess%C3%B3rios!5e0!3m2!1spt-BR!2sbr!4v1571182221918!5m2!1spt-BR!2sbr" width="100%" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe></p>
			  </div>
			</div>
		</div>
	</div>
</div>