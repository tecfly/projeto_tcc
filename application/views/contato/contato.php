<div class="fundo mt-5">
	<section class="text-center container">

      <div class="row mx-1 mt-5">
          <div class="col-lg-4 col-md-12 mb-4">
              <div class="card testimonial-card">
                  <div class="card-up morpheus-den-gradient">
                  </div>
                  <div class="avatar mx-auto"><img src="<?= base_url('assets/img/location.png')?>" class="rounded-circle img-responsive" alt="Example photo">
                  </div>
                  <div class="card-body">
                      <h4 class="card-title mt-1" style="font-weight:bold;">Endereço</h4>
                      <hr  style="background-color:Blue;">
                      <p><i class="fas fa-map-marked-alt"></i> Endereço: R. Sen. Nilo Coelho, 407 - Conj. Inocoop - Guarulhos - SP, <br>CEP 07174-010</p>
                  </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-12 mb-4">
              <div class="card testimonial-card">
                  <div class="card-up morpheus-den-gradient">
                  </div>
                  <div class="avatar mx-auto"><img src="<?= base_url('assets/img/telefone.png')?>" class="rounded-circle img-responsive" alt="Example photo">
                  </div>
                  <div class="card-body">
                      <h4 class="card-title mt-1" style="font-weight:bold;">Telefone</h4>
                      <hr style="background-color:Blue;">
                      <p><i class="fas fa-phone-square"></i> (11) 2600-7454<br>
					  <i class="fas fa-phone-square"></i> (11) 98498-1782<br>
					  <i class="fas fa-phone-square"></i> (11) 94753-8042
					  </p>
                  </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-12 mb-4">
              <div class="card testimonial-card">
                  <div class="card-up morpheus-den-gradient"></div>
                  <div class="avatar mx-auto"><img src="<?= base_url('assets/img/redessociais.png')?>" class="rounded-circle img-responsive" alt="Example photo">
                  </div>
                  <div class="card-body">
                      <h4 class="card-title mt-1" style="font-weight:bold;">Redes Sociais</h4> 
                      <hr style="background-color:Blue;">
                      <p><i class="fab fa-instagram"></i> Instagram: elisomautopecas_Oficial<br>
					  <i class="fab fa-facebook-square"></i> Facebook: Elisom Autopeças<br>
					  <i class="fas fa-envelope"></i> Email: elisomautopecas@gmail.com
                  </div>
              </div>
          </div>
      </div>
  </section>
</div>