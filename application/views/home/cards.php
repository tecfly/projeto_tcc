<div class="container">
    <div class="row">
        <?php foreach ($promocao as $promocao):?>
        <div class="card-deck mx-auto col-md-3 mb-4">
            <div class="container card mx-auto mb-4">
                <div class=" container mt-4 text-center">
                        <img width="200" height="150" src="<?= base_url('uploads/prod/'). $promocao->foto ?>" class="">
                    <p class="text-center" style="font-size:11px;color:grey;">Imagem ilustrativa</p>
                </div>
                <div class="mx-auto container mt-3">
                    <h4 style="font-weight:bold;" class="card-title text-center"> <?= $promocao->nome ?> </h4>
                        <center><p style="font-family:dorsis;font-size:23px;"><?= $promocao->descricao ?><p></center>
                    <h4 class="font-weight-bold blue-text text-center mb-5">
                        <strong class="mb-4">R$<?= $promocao->preco ?></strong>
                    </h4> 
                </div>
            </div>
        </div>
        <?php endforeach;?>
	</div>
</div>