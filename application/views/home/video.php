 <link rel="stylesheet" href="<?= base_url('assets/mdb/css/home.css')?>">
<div class="view">
    <video class="video-intro" poster="https://mdbootstrap.com/img/Photos/Others/background.jpg" playsinline autoplay
      muted loop>
      <source src="https://mdbootstrap.com/img/video/Lines-min.mp4" type="video/mp4">
    </video>
    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
      <div class="text-center white-text mx-5 wow fadeIn">
        <center>
            <img src="<?= base_url('assets/img/logotipo_oficial.png') ?>" width="120" height="120"/>
        </center>
        <h3 class="display-4 mb-4">
          <strong style="font-family:dorsis; font-weight:bold;">Autopeças Elisom</strong>
        </h3>
        <p id="time-counter" class="border border-light my-4"></p>
        <h4 class="mb-4">
          <strong></strong>
        </h4>
        <h4 class="mb-4 d-none d-md-block">
          <strong style="font-family:dorsis;">Proporcionando longevidade ao seu automóvel!</strong>
        </h4>
        <a style="font-family:dorsis;" href="<?= base_url('index.php/principal/sobre')?>" class="btn btn-outline-white btn-lg">Sobre nós
            <i class="fas fa-users"></i>
        </a>
      </div>
    </div>
</div>