<footer class="black pt-4">
  <hr class="my-4 primary-color">
  <div class="pb-4">
    <center>
      <a href="" title="elisomautopeças@gmail.com" target="_blank">
        <i class="far fa-envelope mr-3 fa-2x"></i>
      </a>

      <a href="https://www.facebook.com/elisom.autopecas.1" title="Elisom Autopeças" target="_blank">
        <i class="fab fa-facebook-f mr-3 fa-2x"></i>
      </a>

      <a href="https://www.instagram.com/elisomautopecas_oficial/" title="elisomautopecas_oficial" target="_blank">
        <i class="fab fa-instagram mr-3 fa-2x"></i>
      </a>
    </center>
  </div>

  <div class="footer-copyright black text-primary py-3">
    <center>
      © <?= date("Y")?>: 
      <a href="<?= base_url('index.php') ?>" target="_blank">Autopeças Elisom </a>
    </center>
  </div>

</footer>