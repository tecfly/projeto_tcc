<html lang="br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Autopeças Elisom</title>

  <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/img/logotipo_oficial.png') ?>">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <link rel='stylesheet' href='https://mdbootstrap.com/wp-content/themes/mdbootstrap4/css/compiled-4.8.10.min.css?ver=4.8.10'>
  <link rel='stylesheet' id='compiled.css-css'  href='https://mdbootstrap.com/wp-content/themes/mdbootstrap4/css/compiled-4.8.10.min.css?ver=4.8.10' type='text/css' media='all' />
  <link rel='stylesheet' id='whatsappme-css'  href='https://www.duplod.com.br/wp-content/plugins/creame-whatsapp-me/public/css/whatsappme.css' type='text/css' media='all' />
  <link rel='stylesheet' id='compiled.css-css'  href='https://mdbootstrap.com/wp-content/themes/mdbootstrap4/css/compiled-4.8.11.min.css?ver=4.8.11' type='text/css' media='all' />
  <script type='text/javascript' src='https://www.duplod.com.br/wp-includes/js/jquery/jquery.js'></script>	
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>
  <script type="text/javascript" src="<?= base_url('assets/mdb/js/tabela.js')?>"></script>

  <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.11.2/css/all.css'>

  <link href="<?= base_url('assets/mdb/css/bootstrap.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/bootstrap.min.css') ?>" rel="stylesheet">

  <link href="<?= base_url('assets/mdb/css/mdb.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/mdb.min.css' )?>" rel="stylesheet">

  <link href="<?= base_url('assets/mdb/css/mdb.lite.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/mdb.lite.min.css') ?>" rel="stylesheet">

  <link href="<?= base_url('assets/mdb/css/style.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/style.min.css') ?>" rel="stylesheet">

  <link href="<?= base_url('assets/mdb/css/estilos.css')?>"  rel="stylesheet">
 
  <link href="<?= base_url('assets/mdb/scss/style.scss') ?>" rel="stylesheet">

  <link rel="stylesheet" href="<?= base_url('assets/mdb/css/master.css')?>">

  <link rel="stylesheet" href="<?= base_url('assets/mdb/css/all.min.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/mdb/css/dataTables.bootstrap4.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/mdb/css/sb-admin.css')?>">

</head>

  
