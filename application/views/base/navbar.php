<body>
	<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar black" >
		<div class="container">
		  <a style="font-weight:bold; font-family:Dosis; font-size:25px;" class="navbar-brand"  href="<?= base_url('index.php')?>">
			<strong>Autopeças Elisom</strong>
		  </a>

		  <button class="navbar-toggler  text-white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-bars text-white"></i>
		  </button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
				  <li class="nav-item">
					<a class="nav-link" style="font-family:Dosis; font-size:20px;" href="<?= base_url('index.php')?>"> <i class="fas fa-home"></i> Home
					</a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" style="font-family:Dosis; font-size:20px;" href="<?= base_url('index.php/principal/sobre')?>" ><i class="fas fa-users"></i> Sobre nós</a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" style="font-family:Dosis; font-size:20px;" href="<?= base_url('index.php/principal/produtos')?>" ><i class="fas fa-barcode"></i> Produtos</a>
				  </li>
				  <li class="nav-item">
					<a class="nav-link" style="font-family:Dosis; font-size:20px;" href="<?= base_url('index.php/principal/contato')?>" > <i class="fas fa-envelope"></i> Contato</a>
				  </li>
				</ul>
				<ul class="navbar-nav ml-auto nav-flex-icons">
					<li class="nav-item avatar">
						<a class="nav-link p-0" data-toggle="modal" data-target="#modalLoginForm" href="<?= base_url('index.php/adm')?>">
						  <img src="<?= base_url('assets/img/logotipo_oficial.png')?>" class="rounded-circle z-depth-0" alt="avatar image" height="35" >
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>