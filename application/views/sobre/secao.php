<div class="row mx-auto bg-primary mt-4">
  <div class=" col-md-6 mt-5 white">
    <div class="text-black text-center d-flex align-items-center  py-5 px-4">
      <div>
        <h5  style="font-family:Dosis;color:#1a237e; font-weight:bold; "><i class="fas fa-check"></i> Como tudo começou?</h5>
        <h3 class="card-title pt-2" style="font-family:Dosis;font-weight:600;font-style:normal"><strong>História </strong></h3>
		<p class="text-justify" style="font-family:Dosis; font-size:15px;">A história se inicia à muitos anos atrás, quando Wellington Pinheiro de Souza trabalhava com a realização de eventos, tanto na área da gestão como no entretenimento, Wellington fantasiava-se de diversos personagens, como por exemplo de palhaço.</p>
        <p class="text-justify" style="font-family:Dosis; font-size:15px;">Com o decorrer do tempo o atual empreendedor observou a falta de instabilidade no setor de eventos, pois este era um ramo incerto, onde poderia ou não haver solicitações de serviços.</p>
        <a class="btn indigo darken-4" style="font-family:Dosis;" data-toggle="modal" data-target="#basicExampleModal"><i class="fas fa-clone left"></i> Conheça o empreendedor!</a>
      </div>
    </div>
  </div>

  <div class="col-md-6 mt-5">
    <div class="grey-text  text-center text-white d-flex align-items-center  py-5 px-4">
      <div>
        <h5 style="font-family:Dosis;color:#1a237e; font-weight:bold;"><i class="fas fa-check"></i> O que aconteceu?</h5>
        <h3 class="card-title pt-2" style="font-family:Dosis;font-weight:600;font-style:normal"><strong>Realizando um sonho!</strong></h3>
        <p class="text-justify" style="font-family:Dosis; font-size:15px;">Wellington decidiu ajuntar capital, pois seu desejo era alcançar estabilidade financeira, a qual ele traçou um caminho para obter e este caminho era ter o seu próprio negócio.</p>
		<p class="text-justify" style="font-family:Dosis; font-size:15px;">Após muito esforço e dedicação o fundador da empresa Autopeças Elisom, nota que havia chegado o grande momento de investir seu capital de forma ainda mais direta. Atualmente já fazem 9 anos (desde 2000) que o estabelecimento está em funcionamento.</p>  
        <a class="btn indigo darken-4" style="font-family:Dosis;" data-toggle="modal" data-target="#basicExampleModal2"><i class="fas fa-clone left"></i> Conheça a empresa física!</a>
      </div>
    </div>
  </div>

  <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"  style="font-weight:bold;font-family:Dosis;">Empreendedor Wellington Pinheiro</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <center><img height="300" width="300" src="<?= base_url('assets/img/wel.png') ?>" class="img-fluid" ></center>
        </div>
        <div class="modal-footer">
			<div class="mr-auto">
              <i class="fas fa-phone-alt fa-1x mr-3 black-text mb-2"> (11) 2600-7454 </i><br>
              <i class="fas fa-envelope fa-1x black-text"> Elisomproducoes@hotmail.com </i>
			</div>
          <button type="button" class="btn indigo darken-4 text-white"  style="font-family:Dosis;" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="basicExampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold;font-family:Dosis;">Loja Física Autopeças Elisom</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <center><img height="300" width="300" src="<?= base_url('assets/img/loja.png') ?>" class="img-fluid" ></center>
        </div>
        <div class="modal-footer">
          <div class="mr-auto">
              <i class="fas fa-phone-alt fa-1x mr-3 mb-2 black-text"> (11) 2600-7454 </i><br>
              <i class="fas fa-map-marker-alt fa-1x black-text">  Inocoop - Guarulhos - SP, CEP 07174-010, Nº 407</i>
          </div>
          <button type="button" class="btn indigo darken-4 text-white"  style="font-family:Dosis;" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
</div> 


