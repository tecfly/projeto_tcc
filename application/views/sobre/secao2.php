<div class="col-md-12 text-center mx-auto mt-5">
		<div class="row">
			<div class="card text-white text-center black col-md-3">
				<div class="card-body">
					<h5 class="card-title" style="text-align: center; font-family:Dosis;font-size:20px;">Esse sonho que beneficia Wellington é reciproco a sociedade, 
							pois o empreendedor tem grande apresso por seus clientes e funcionários</h5>
				</div>
			</div>
			<div class="card text-white text-center bg-primary col-md-3">
				<div class="card-body">
					<h5 class="card-title" style="text-align: center; font-family:Dosis;font-size:20px;">As melhores peças</h5>
					<i class="fas fa-cogs fa-6x"></i>
				</div>
			</div>
			<div class="card text-white text-center black col-md-3">
				<div class="card-body">
					<h5 class="card-title" style="text-align: center; font-family:Dosis;font-size:20px;">Os melhores acessórios</h5>
					<i class="fas fa-shopping-bag fa-4x"></i>
					<i class="fas fa-car-crash fa-6x"></i>
				</div>
			</div>
			<div class="card text-white text-center bg-primary col-md-3">
				<div class="card-body">
					<h5 class="card-title" style="text-align: center; font-family:Dosis;font-size:20px;">O melhor atendimento</h5>
					<i class="fas fa-comments fa-6x"></i>
				</div>
			</div>
		</div>
</div>