<link rel='stylesheet' id='js_composer_front-css'  href='http://www.nadarte.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.1.1' type='text/css' media='all' />
<div class="row mb-5 col-md-12 mt-5 black mx-auto border border-dark">
	<div  class="col-md-4 mt-5 text-white">	
		<p style="text-align:center;"><i style="font-size:40px;"  class="fa fa-bullseye blue-text  " ></i></p><h3 style="font-size: 36px;color: #0a0a0a;line-height: 1,0;text-align: center;font-family:Dosis;font-weight:600;font-style:normal" class="text-white" >Missão</h3>
			<div class="">
				<p class="text-justify text-white mb-5" style="font-family:Dosis;font-size:17px;">Fazer o atendimento com excelência em busca da satisfação do cliente.</p>
			</div>  
	</div> 
	<div class=" white col-md-4">
		<div  class="mt-5">	
			<p style="text-align:center;"><i style="font-size:40px;"  class="fa fa-binoculars blue-text " ></i></p><h3 style="font-size: 36px;color: #0a0a0a;line-height: 1,0;text-align: center;font-family:Dosis;font-weight:600;font-style:normal" class="vc_custom_heading" >Visão</h3>
			<div class="">
				<p class="text-justify mb-5" style="font-family:Dosis; font-size:17px;">Ser referência no mercado de autopeças como a melhor da região, buscando sempre um avanço tecnológico.</p>
			</div> 
		</div> 
	</div>
	<div class="col-md-4 mt-5 text-white">	
		<p style="text-align:center;"><i style="font-size:40px;"  class="fa fa-anchor blue-text " ></i></p><h3 style="font-size: 36px;color: #0a0a0a;line-height: 1,0;text-align: center;font-family:Dosis;font-weight:600;font-style:normal" class="text-white" >Valores</h3>
		<div class="text-white">
			<p style="text-align: center; font-family:Dosis;font-size:17px;">Educação, honestidade e credibilidade.</p>
		</div> 
	</div> 
</div>
