<?php
include_once APPPATH. 'libraries/util/CI_Object.php';
    
class Calendar extends CI_Object {

    public function cria_calendar($data){
        $this->db->insert('calendar', $data);
    }
    
    public function lista_calendar(){
        $this->db->select('id, date, content');
        $rs = $this->db->get('calendar'); 
        $result = $rs->result_array();
        return $result;
    }

    public function cadastra_calendar($data){
        $this->db->insert('calendar', $data);
    }

    public function calendar_data($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('calendar', $cond);
        return $rs->row_array();
    }

    public function edita_calendar($data, $id){
        $this->db->update('calendar', $data, "id = $id");
    }

    public function delete_calendar($id){
        $cond = array('id' => $id);
        $this->db->delete('calendar', $cond);
    }

}
