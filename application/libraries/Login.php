<?php
include_once APPPATH. 'libraries/util/CI_Object.php';

class Login extends CI_Object {

    public function can_login($email, $senha){

        $this->db->where('email', $email);
        $this->db->where('senha', $senha);
        $query = $this->db->get('usuario');

        if($query->num_rows() > 0){
            return true;
        }
        else {
            return false;
        }
    }
}
