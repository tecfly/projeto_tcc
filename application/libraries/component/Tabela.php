<?php 
include_once 'Component.php';

class Tabela extends Component {
    //ATRIBUTOS
    private $data;
    private $label;

    //CLASSES DO CABEÇALHO DA TABELA
    private $header_classes = '';

    //CLASSES DO CORPO DA TABELA
    private $body_classes = '';

    //Botao 
    private $use_action_button = false;
    private $use_action_button2 = false;
    private $use_action_button3 = false;
    private $use_action_button4 = false;

    //CONSTRUTOR
    function __construct(array $data, array $label){
        $this->label = $label;
        $this->data = $data;
    }

    //METODOS
    function getHTML(){
        $html = '<table class=" table '.$this->body_classes.'" style="font-family:arial;">';
        $html .= $this->header();
        $html .= $this->body();
        $html .= '</table>';
        return $html;
    }

    public function addHeaderClass($class){
        $this->header_classes .= $class.' ';
    }

    public function addBodyClass($class){
        $this->body_classes .= $class.' ';
    }

    private function header(){
        $html = '<thead class="'.$this->header_classes.'"><tr>';
        foreach ($this->label as $name){
            $html .= '<th scope="col">'.$name.'</th>';
        }

        if($this->use_action_button){
            $html .= '<th scope="col">Editar</th>';
            $html .= '<th scope="col">Deletar</th>';
        }

        if($this->use_action_button2){
            $html .= '<th scope="col">Editar</th>';
            $html .= '<th scope="col">Deletar</th>';
        }

        if($this->use_action_button3){
            $html .= '<th scope="col">Data/Hora</th>';
            $html .= '<th scope="col">Deletar</th>';
        }

        if($this->use_action_button4){
            $html .= '<th scope="col">Editar</th>';
            $html .= '<th scope="col">Deletar</th>';
        }

        $html .= '</tr><thead>';
        return $html;
    }

    private function body(){
        $html = '<tbody>';
        foreach ($this->data as $row){
            $html .= '<tr>';

            foreach ($row as $cel) {
                $html .= '<td>'.$cel.'</td>';
            }

            if($this->use_action_button){
                $html .= '<td>'.$this->action_buttons($row['id']).'</td>';
                $html .= '<td>'.$this->action_buttons2($row['id']).'</td>';
            }

            if($this->use_action_button2){
                $html .= '<td>'.$this->action_buttons3($row['id']).'</td>';
                $html .= '<td>'.$this->action_buttons4($row['id']).'</td>';
            }

            if($this->use_action_button3){
                $html .= '<td>'.$this->action_buttons5($row['id']).'</td>';
            }

            if($this->use_action_button4){
                $html .= '<td>'.$this->action_buttons6($row['id']).'</td>';
                $html .= '<td>'.$this->action_buttons7($row['id']).'</td>';
            }

            $html .= '</tr>';
        }
        
        $html .= '</tbody>';
        return $html;
    }

    //Categoria
    private function action_buttons($id){
        $html = '<a href="'.base_url('index.php/categoria/editar/'.$id). '">';
        $html .= '<button type="button"
        class="btn btn-info btn-rounded btn-sm my-0">Editar</button></a>';
        return $html;
    }

    private function action_buttons2($id){
        $html = '<a href="'.base_url('index.php/categoria/deletar/'.$id). '">';
        $html .= '<button type="button"
        class="btn btn-danger btn-rounded btn-sm my-0">Deletar</button></a>';
        return $html;
    }

    //Calendário
    private function action_buttons3($id){
        $html = '<a href="'.base_url('index.php/mycalendario/editar/'.$id). '">';
        $html .= '<button type="button"
        class="btn btn-info btn-rounded btn-sm my-0">Editar</button></a>';
        return $html;
    }

    private function action_buttons4($id){
        $html = '<a href="'.base_url('index.php/mycalendario/deletar/'.$id). '">';
        $html .= '<button type="button"
        class="btn btn-danger btn-rounded btn-sm my-0">Excluir</button></a>';
        return $html;
    }

    //Mensagens
    private function action_buttons5($id){
        $html = '<a href="'.base_url('index.php/adm/deletar/'.$id). '">';
        $html .='<button type="button"
        class="btn btn-danger btn-rounded btn-sm my-0">Excluir</button></a>';
        return $html;
    }

    //Usuário
    private function action_buttons6($id){
        $html = '<a href="'.base_url('index.php/adm/editar_usuario/'.$id). '">';
        $html .= '<button type="button"
        class="btn btn-info btn-rounded btn-sm my-0">Editar</button></a>';
        return $html;
    }

    private function action_buttons7($id){
        $html = '<a href="'.base_url('index.php/adm/deletar_usuario/'.$id). '">';
        $html .= '<button type="button"
        class="btn btn-danger btn-rounded btn-sm my-0">Excluir</button></a>';
        return $html;
    }
    

    public function useZebra(){
        $this->body_classes .= 'table-striped ';
    }

    public function useBorder(){
        $this->body_classes .= 'table-bordered ';
    }

    public function useHover(){
        $this->body_classes .= 'table-hover ';
    }

    public function smallRow(){
        $this->body_classes .= 'table-sm ';
    }

    public function useActionButton(){
        $this->use_action_button = true;
    }

    public function useActionButton2(){
        $this->use_action_button2 = true;
    }

    public function useActionButton3(){
        $this->use_action_button3 = true;
    }

    public function useActionButton4(){
        $this->use_action_button4 = true;
    }

}
