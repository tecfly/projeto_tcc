<?php
include_once APPPATH. 'libraries/util/CI_Object.php';
    
class Categoriadb extends CI_Object {

    public function get_all(){
        $rs = $this->db->get('categoria');
        return $rs->result_array();
    }

    public function cria($categoria){
        $this->db->insert('categoria', $categoria);
        return $this->db->insert_id();
    }

    public function get_name($id){
        $cond['id'] = $id;
        $rs = $this->db->get_where('categoria', $cond);
        $categoria = $rs->row();

        return $categoria->categoria;
    }
    
    public function lista_categoria(){
        $rs = $this->db->get('categoria'); 
        $result = $rs->result_array();
        return $result;
    }

    public function cadastra_categoria($data){
        $this->db->insert('categoria', $data);
    }

    public function categoria_data($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('categoria', $cond);
        return $rs->row_array();
    }

    public function edita_categoria($data, $id){
        $this->db->update('categoria', $data, "id = $id");
    }

    public function delete_categoria($id){
        $cond = array('id' => $id);
        $this->db->delete('categoria', $cond);
    }

}
