<?php
include_once APPPATH. 'libraries/util/CI_Object.php';
    
class Cadastro extends CI_Object {

    public function lista_usuario(){
        $this->db->select('id, nome, sobrenome, email');
        $rs = $this->db->get('usuario'); 
        $result = $rs->result_array();
        return $result;
    }

    public function criando_usuario($data){
        $this->db->insert('usuario', $data);
    }

    public function user_data($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('usuario', $cond);
        return $rs->row_array();
    }

    public function edita_usuario($data, $id){
        $this->db->update('usuario', $data, "id = $id");
    }

    public function delete($id){
        $cond = array('id' => $id);
        $this->db->delete('usuario', $cond);
    }
    
}
