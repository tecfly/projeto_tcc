<?php
include_once APPPATH. 'libraries/util/CI_Object.php';
    
class Entradadb extends CI_Object {

    public function lista_entrada(){
        $this->db->select('id, nome, categ, quantidade');
        $rs = $this->db->get('produtos'); 
        $result = $rs->result_array();
        return $result;
    }

    public function cadastra_entrada($data){
        $this->db->insert('produtos', $data);
    }

    public function entrada_data($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('produtos', $cond);
        return $rs->row_array();
    }

    public function entrada(){
        $sql= "SELECT id, id_produto, nome, sum(quant) AS quantidade FROM movimentos,produtos
        WHERE quant > 0 and id_produto=id GROUP by nome ";
        $rs = $this->db->query($sql);
        return $rs->result();
    }

    public function saida(){
        $sql= "SELECT id, id_produto, nome, sum(quant) AS quantidade FROM movimentos,produtos
        WHERE quant < 0 and id_produto=id GROUP by nome ";
        $rs = $this->db->query($sql);
        return $rs->result();
    }
}
