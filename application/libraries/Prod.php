<?php
include_once APPPATH. 'libraries/util/CI_Object.php';

class Prod extends CI_Object {
    
    public function get_all(){
        $rs = $this->db->get('produtos');
        return $rs->result_array();
    }

    public function lista_produtos_adm($id){
        $cond['categ'] = $id;
        $rs = $this->db->get_where('produtos', $cond);
        return $rs->result();
    }

    public function lista_produtos($id){
        $cond['categ'] = $id;
        $rs = $this->db->get_where('produtos', $cond);
        return $rs->result();
    }
    
    public function produtos_data($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('produtos', $cond);
        return $rs->row_array();
    }

    public function edita_produtos($data, $id){
        $this->db->update('produtos', $data, "id = $id");
    }

    public function delete_produtos($id){
        $cond = array('id' => $id);
        $this->db->delete('produtos', $cond);
    }
    

    public function promocao_ativa(){
        $sql= "SELECT * FROM produtos WHERE promocao = 1";
        $rs = $this->db->query($sql);
        return $rs->result();
    }

    public function gera($produtos) {
        $this->db->insert('produtos', $produtos);
        return $this->db->insert_id();
    }
}
