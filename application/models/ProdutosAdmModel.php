<?php
include_once APPPATH. 'libraries/Prod.php';

class ProdutosAdmModel extends CI_Model {

    public function cria_produto(){
        if(sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('produtos[nome]', 'Nome do Produto', 'trim|required');
        $this->form_validation->set_rules('produtos[categ]', 'Nome da Categoria', 'trim|required|is_natural');
        $this->form_validation->set_rules('produtos[descricao]', 'Descrição do Produto', 'trim|required');
        $this->form_validation->set_rules('produtos[preco]', 'Preço do Produto', 'trim|required');
        $this->form_validation->set_rules('produtos[quantidade]', 'Quantidade em Estoque', 'trim|required|is_natural');
    
        if (empty($_FILES['foto']['name'])){
             $this->form_validation->set_rules('foto', 'Foto', 'required');
        }

        if($this->form_validation->run() == true) {

            $produtos = $this->input->post('produtos');
            $config [ 'upload_path' ] = './uploads/prod'; 
            $config [ 'allowed_types' ] = 'jpg|png|jpeg';
            $config [ 'encrypt_name' ] =  true;

            $this->load->library('upload', $config);
        
            if ($this->upload->do_upload('foto')) {
                $arq = $this->upload->data();
                $arq = $arq['file_name'];
                $produtos['foto'] = $arq;
                $prod = new Prod();
                $prod->gera($produtos);

                redirect( base_url('index.php/produtos/cadastroprod?cadastrap=1'));
            }

        }

    }

    public function busca_categoria(){
        return $this->db->get('categoria')->result_array();
    }


    public function edita_produtos($id){
        $produtos = new Prod();
        $info = $produtos->produtos_data($id);

        if(sizeof($_POST) == 0) return;

        $data = $this->input->post('produtos');
        
            $config [ 'upload_path' ] = './uploads/prod/'; 
            $config [ 'allowed_types' ] = 'jpg|png|jpeg';
            $config [ 'encrypt_name' ] =  true;
            
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {
                $arquivo = './uploads/prod/' . $info['foto'];
                if(file_exists($arquivo)){
                    unlink($arquivo);
                }
                $arq = $this->upload->data();
                $arq = $arq['file_name'];
                $data['foto'] = $arq;
                
            }

        $produtos->edita_produtos($data, $id);
        redirect('produtos/pesquisar');
    }
    
    public function read($id){
        $Prod = new Prod();
        return $Prod->produtos_data($id);
    }

    public function deletar($id){
        $Prod = new Prod();
        $Prod->delete_produtos($id);
    }

    public function lista_produtos_adm($id){
        $html = '';
        $prod = new Prod();
        $lista = $prod->lista_produtos_adm($id);

        foreach ($lista as $produtos){
            $html .= $this->produtos_display_adm($produtos);
        }
        return $html;
    }

    private function produtos_display_adm($produtos){

        $html = '<div class="card-deck mx-auto col-lg-3 mb-4">
                    <div class="card mx-auto ">
                    <div class="container card mx-auto ">
                        <div class="container mt-4 text-center mx-auto ">
                        <center><img width="200" height="150" src="'.base_url('uploads/prod/').$produtos->foto.'" class=""></center>
                            <p class="text-center" style="font-size:11px;color:grey;">Imagem ilustrativa</p>
                        </div>
                        <div class="mx-auto container mt-5">
                            <h4 style="font-weight:bold;" class="card-title text-center"> '.$produtos->nome.' </h4>
                            <h4 class="font-weight-bold indigo-text text-center">
                            <strong>R$'.$produtos->preco.'</strong>
                            </h4> 
                            <center><h5 class="card-title"><i class="fas fa-shopping-bag grey-text mr-3"></i>Estoque: '.$produtos->quantidade.' </h5></center>
                            <a class="center"> 
                            <center><button type="button" class="btn indigo darken-4 white-text mb-3" data-toggle="modal" 
                                data-target="#basicExampleModal_'.$produtos->id.'">Detalhes</button></center> 
                            </a>
                        </div>
                    </div>
                    <div class="modal fade" id="basicExampleModal_'.$produtos->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 style="color:#00008B;" class="modal-title" id="exampleModalLabel"> <i class="fas fa-angle-double-right"></i> Descricão de "'.$produtos->nome.'" </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div style="font-weight:bold;" class="modal-body text-center">
                                   <p> '.$produtos->descricao.'</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn indigo darken-4 text-white" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
        return $html;

        if($produtos['promocao'] == "on") {
            $this->db->insert('produtos');
        }

    }

    public function gera_table_like($value, $reg_p_pag){
        $termo = $this->input->post('busca');
        $this->db->select('*');
        $this->db->like('nome', $termo);
        $this->db->limit($reg_p_pag, $value);
        return $this->db->get('produtos')->result();
    }

    function get_qtd_produtos(){
        $this->db->select('count(*) as total');
        return $this->db->get('produtos')->result();
    }

    public function getProdutoByID($id=NULL)
    {
    if ($id != NULL):
        $this->db->where('id', $id);        
        $this->db->limit(1);
        $query = $this->db->get("produtos");        
        return $query->row();   
    endif;
    } 

    public function promocaoProduto($promocao=NULL, $id=NULL){
        if ($promocao != NULL && $id != NULL):
            $this->db->update('produtos', $promocao, array('id'=>$id));            
        endif;
    }
    
    public function promocao(){
        $prod= new Prod();
        return $prod->promocao_ativa();
    }

}

