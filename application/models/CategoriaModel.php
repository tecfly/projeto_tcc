<?php 
include_once APPPATH. 'libraries/Categoriadb.php';
include_once APPPATH. 'libraries/component/Tabela.php';

class CategoriaModel extends CI_Model{

    public function gera_table(){
        $html = '';
        $categoriadb = new Categoriadb();
        $data = $categoriadb->lista_categoria();

        $label = ['Nº', 'Categoria', 'Foto'];
        $table = new Tabela ($data, $label);
        $table->addHeaderClass('indigo darken-4 white-text text-center');
        $table->addBodyClass('text-center border');
        $table->useHover();
        $table->smallRow();
        $table->useActionButton();
        return $table->getHTML();
    }


    public function edita_categoria($id){
        $categoria = new Categoriadb();
        $info = $categoria->categoria_data($id);

        if(sizeof($_POST) == 0) return;

        $data = $this->input->post('categoria');
        
            $config [ 'upload_path' ] = './uploads/categ/'; 
            $config [ 'allowed_types' ] = 'jpg|png|jpeg';
            $config [ 'encrypt_name' ] =  true;
            
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {
                $arquivo = './uploads/categ/' . $info['foto'];
                if(file_exists($arquivo)){
                    unlink($arquivo);
                }
                $arq = $this->upload->data();
                $arq = $arq['file_name'];
                $data['foto'] = $arq;
            }
    
        $categoria->edita_categoria($data, $id);
        redirect('categoria/lista');
    }
    
    public function read($id){
        $categoriadb = new Categoriadb();
        return $categoriadb->categoria_data($id);
    }

    public function deletar($id){
        $categoriadb = new Categoriadb();
        $categoriadb->delete_categoria($id);
    }

    public function lista_categoria_adm(){
        $html = '';
        $categoriadb = new Categoriadb();
        $v = $categoriadb->get_all();

        foreach ($v AS $categoria){
            $html .= $this->categ_display_adm($categoria);
        }
        return $html;
    }

    private function categ_display_adm($categoria){
        $html = '
        <div class="card-deck mx-auto col-md-4 mb-4">
        
            <div class="card mx-auto"><span  class="border">
                <div class="container mt-4">
                
                <h5 style="font-weight:bold;font-family:Dosis;font-size:30px;" class="card-title text-center"> '.$categoria['categoria'].' </h5>
                <center><img class="card-img-top mt-5 mb-2 " width="200" height="200" src="'.base_url('uploads/categ/'.$categoria['foto']).'"></center>
                <p class="text-center" style="font-size:11px;color:grey;">Imagem ilustrativa</p>

                </div>
                <div class="mx-auto mt-5">
                <a class="center" href="'.base_url('index.php/produtos/detalhe/'.$categoria['id']).'"> 
                <center><button type="button" class="btn indigo darken-4 white-text mb-5">Visualizar</button></center>
                </a>
                </div>
                </span></div>
        
        </div>
        ';
        return $html;
    }

    public function nome_categoria($id){
        $categoriadb = new Categoriadb();
        return $categoriadb->get_name($id);
    }

    public function cria_categoria(){
        if(sizeof($_POST) == 0) return;
        
        $this->form_validation->set_rules('categoria[categoria]', 'Nome da categoria do produto', 'trim|required');
        if (empty($_FILES['foto']['name'])){
            $this->form_validation->set_rules('foto', 'Foto', 'required');
        }
        
        if($this->form_validation->run() == true){ 
            $categoria = $this->input->post('categoria');
        
            $config [ 'upload_path' ] = './uploads/categ/'; 
            $config [ 'allowed_types' ] = 'jpg|png|jpeg';
            $config [ 'encrypt_name' ] =  true;
            
            $this->load->library('upload', $config);


            if ($this->upload->do_upload('foto')) {
                $arq = $this->upload->data();
                $arq = $arq['file_name'];
                $categoria['foto'] = $arq;
                $cproduto = new Categoriadb();
                $cproduto->cria($categoria);

                redirect( base_url('index.php/categoria/index?cadastrado=1'));
            }
        }
    }
}
