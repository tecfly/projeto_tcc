<?php
include_once APPPATH. 'libraries/Calendar.php';
include_once APPPATH. 'libraries/component/Tabela.php';
    
class CalendarioModel extends CI_Model {

	public $prefs;

	public function __construct()
	{
		//parent::Model();
		$this->prefs = array(
        'start_day'    => 'saturday',
        'month_type'   => 'long',
        'day_type'     => 'short',
        'show_next_prev' => TRUE,
        'next_prev_url'   => base_url('index.php/mycalendario/index/')
		);
		$this->prefs['template'] = '
		{table_open}
			<table border="0" cellpadding="0" cellspacing="0" class="calender">{/table_open}

        {heading_row_start}<tr>{/heading_row_start}

        {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
        {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

        {heading_row_end}</tr>{/heading_row_end}

        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td>{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}

        {cal_row_start}<tr class="days">{/cal_row_start}
        {cal_cell_start}<td>{/cal_cell_start}
        {cal_cell_start_today}<td>{/cal_cell_start_today}
        {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}

        {cal_cell_content}
        	<div class="day_num">{day}</div>
        	<div class="content">{content}</div>
        {/cal_cell_content}
        {cal_cell_content_today}
        <div class="">
        	<div class="day_num highlight">{day}</div>
        	<div class="content">{content}</div>
    	</div>
    	{/cal_cell_content_today}

        {cal_cell_no_content}{day}{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}

        {cal_cell_blank}&nbsp;{/cal_cell_blank}

        {cal_cell_other}{day}{/cal_cel_other}

        {cal_cell_end}</td>{/cal_cell_end}
        {cal_cell_end_today}</td>{/cal_cell_end_today}
        {cal_cell_end_other}</td>{/cal_cell_end_other}
        {cal_row_end}</tr>{/cal_row_end}

        {table_close}</table>{/table_close}
		';
	}

	public function getcalender($year , $month)
	{
		$this->load->library('calendar',$this->prefs); 
		$data = $this->get_calender_data($year,$month);
		return $this->calendar->generate($year , $month , $data);
	}

	public function get_calender_data($year , $month)
	{
		$query =  $this->db->select('date,content')->from('calendar')->like('date',"$year-$month",'after')->get();
		$cal_data = array();
		foreach ($query->result() as $row) {
            $calendar_date = date("Y-m-j", strtotime($row->date)); 
			$cal_data[substr($calendar_date, 8,2)] = $row->content;
		}
		
		return $cal_data;
	}

	public function add_calendar_data($data , $date)
	{
		$this->db->insert('calendar',array(
			'date'	=> $date,
			'content'	=> $data,
		));
	}
	
    public function gera_calendario(){
        $html = '';
        $Calendar = new Calendar();
        $data = $Calendar->lista_calendar();
    
        $label = ['Nº', 'Data', 'Assunto'];
        $table = new Tabela ($data, $label);
        $table->addHeaderClass('indigo darken-4 white-text text-center');
        $table->addBodyClass('text-center border');
        $table->useHover();
        $table->smallRow();
        $table->useActionButton2();
        return $table->getHTML();
    }

    public function gera_listacalendario(){
        $html = '';
        $Calendar = new Calendar();
        $data = $Calendar->lista_calendar();
        
        $label = ['Nº', 'Data', 'Assunto'];
        $table = new Tabela ($data, $label);
        $table->smallRow();
        return $table->getHTML();
    }
    
    public function cadastra_calendario(){
        if(sizeof($_POST) == 0) return;
    
        $this->form_validation->set_rules('date', 'Data de entrega do serviço', 'trim|required');
        $this->form_validation->set_rules('content', 'Assunto do serviço', 'trim|required|min_length[5]|max_length[50]');
    
        if($this->form_validation->run()){
            $data = $this->input->post();
            $calendar = new Calendar();
            $calendar->cria_calendar($data);
         
            redirect( base_url('index.php/mycalendario/cadastra?cadastrado=1'));   
        }
    }
    
    public function edita_calendar($id){
        if(sizeof($_POST) == 0) return;
    
        $data = $this->input->post();
        $Calendar = new Calendar();
        $Calendar->edita_calendar($data, $id);
        redirect('mycalendario/lista');
    }
    
    public function read($id){
        $Calendar = new Calendar();
        return $Calendar->calendar_data($id);
    }

    public function deletar($id){
        $Calendar = new Calendar();
        $Calendar->delete_calendar($id);
    }

}
