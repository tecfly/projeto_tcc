<?php
include_once APPPATH. 'libraries/Prod.php';
include_once APPPATH. 'libraries/Categoriadb.php';

class ProdutosModel extends CI_Model {

    public function lista_categoria(){
        $html = '';
        $categoriadb = new Categoriadb();
        $v = $categoriadb->get_all();

        foreach ($v AS $categoria){
            $html .= $this->categ_display($categoria);
        }
        return $html;
    }

    private function categ_display($categoria){
        $html = '
        <div class="card-deck mx-auto col-md-4 mb-4">
            <div class="card mx-auto "><span class="border">
                <div class="container mt-4">
                    <h5 class="card-title black-text text-center" style="font-weight:bold;font-family:Dosis;font-size:30px;">  '.$categoria['categoria'].' </h5>
                    <hr style="border:1px solid; border-color:#1a237e; "></hr>
                    <center><img class="card-img-top mt-5 mb-2 " width="200" height="200" src="'.base_url('uploads/categ/'.$categoria['foto']).'"></center>
                    <p class="text-center" style="font-size:11px;color:grey;">Imagem ilustrativa</p>

                </div>
                <div class="mx-auto mt-5">
                    <a class="center" href="'.base_url('index.php/principal/detalhe/'.$categoria['id']).'"> 
                        <center><button type="button" class="btn btn indigo darken-4 white-text mb-5"><i class="far fa-eye"></i> Visualizar</button></center>
                    </a>
                </div>
                </span></div>
        </div>
        ';
        return $html;
    }

    public function nome_categoria($id){
        $categoriadb = new Categoriadb();
        return $categoriadb->get_name($id);
    }

    public function lista_produtos($id){
        $html = '';
        $prod = new Prod();
        $lista = $prod->lista_produtos($id);

        foreach ($lista as $produtos){
            $html .= $this->produtos_display($produtos);
        }
        return $html;
    }

    private function produtos_display($produtos){
        $html = '<div class="card-deck mx-auto col-md-3 mb-4">
                    <div class="container card mx-auto ">
                        <div class=" container mt-4">
                            <center><img width="200" height="150" src="'.base_url('uploads/prod/').$produtos->foto.'" class=""></center>
                            <p class="text-center" style="font-size:11px;color:grey;">Imagem ilustrativa</p>

                        </div>
                        <div class="mx-auto container mt-5">
                            <h4 style="font-weight:bold;" class="card-title text-center"> '.$produtos->nome.' </h4>
                            <h4 class="font-weight-bold indigo-text text-center">
                            <strong>R$'.$produtos->preco.'</strong>
                            </h4> 
                            <a class="center"> 
                            <center><button type="button" class="btn btn indigo darken-4 white-text mb-3" data-toggle="modal" 
                                data-target="#basicExampleModal_'.$produtos->id.'">Detalhes</button></center> 
                            </a>
                        </div>
                    </div>
                    <div class="modal fade" id="basicExampleModal_'.$produtos->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" style="font-family:Dosis;color:#00008B;" id="exampleModalLabel"><i class="fas fa-angle-double-right"></i> Descricão de "'.$produtos->nome.'"</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div style="font-weight:bold;" class="modal-body text-center">
                                <p> '.$produtos->descricao.'</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn indigo darken-4 text-white" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';
        return $html;
    }


    public function cria_categoria(){
        if(sizeof($_POST) == 0) return;

        $categoria = $this->input->post('categoria');
        $categoriadb = new Categoriadb();
        $categoriadb->cria($categoria);
    }  
    
}
