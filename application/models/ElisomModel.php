<?php 
include_once APPPATH. 'libraries/Forms.php';
include_once APPPATH. 'libraries/component/Tabela.php';

class ElisomModel extends CI_Model{

    public function gera_tabela(){
        $html = '';
        $forms = new Forms();
        $data = $forms->lista();

        $label = ['Nº', 'Nome', 'Email', 'Telefone', 'Mensagem' ];
        $table = new Tabela ($data, $label);
        $table->addHeaderClass('indigo darken-4 white-text text-center');
        $table->addBodyClass('text-center border');
        $table->useHover();
        $table->smallRow();
        $table->useActionButton3();
        return $table->getHTML();
    }

    public function salva_usuario(){
        if(sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('nome', 'Nome do usuário', 'trim|required|min_length[8]|max_length[50]');
        $this->form_validation->set_rules('email', 'Email do usuário', 'trim|required|valid_email');
        $this->form_validation->set_rules('telefone', 'Telefone do usuário', 'trim|required|min_length[12]|max_length[13]');
        $this->form_validation->set_rules('mensagem', 'Mensagem do usuário', 'trim|required|min_length[10]|max_length[100]');

        if($this->form_validation->run()){
            $data = $this->input->post();
            $forms = new Forms();
            $forms->cria_usuario($data);
            
            redirect( base_url('index.php/principal/contato?enviado=1'));
        }
    }

    public function edita_usuario($id){
        if(sizeof($_POST) == 0) return;

        $data = $this->input->post();
        $forms = new Forms();
        $forms->edita_usuario($data, $id);
        redirect('adm/lista');
    }

    public function read($id){
        $forms = new Forms();
        return $forms->user_data($id);
    }

    public function deletar($id){
        $forms = new Forms();
        $forms->delete($id);
    }

}
