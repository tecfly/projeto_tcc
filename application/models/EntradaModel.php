<?php 
include_once APPPATH. 'libraries/estoque/Entradadb.php';
include_once APPPATH. 'libraries/component/Tabela.php';

class EntradaModel extends CI_Model{

    public function gera_table($value, $reg_p_pag){
        $this->db->limit($reg_p_pag, $value);
        return $this->db->get('produtos')->result();
    }

    function get_qtd_produtos(){
        $this->db->select('count(*) as total');
        return $this->db->get('produtos')->result();
    }

    public function dados_produto($id){
        return $this->db->from('produtos')->where('id', $id)->get()->result_array();
    } 

    public function movimentos(){
        $resultados = $this->db->select('m.*, p.nome nome, p.quantidade temp')
                               ->from('movimentos as m')
                               ->join('produtos as p', 'm.id_produto = p.id', 'left')
                               ->get();
        return $resultados->result_array();
    }
    
    public function alterar_quantidade($id, $quantidade){
        $this->db->where('id', $id)
                 ->set('quantidade', 'quantidade + '.$quantidade, FALSE)
                 ->update('produtos');

        $dados = array(
            'id_produto'    => $id,
            'quant'    => $quantidade,
        );
        $this->db->insert('movimentos', $dados);
    }    

    public function read($id){
        $entradadb = new Entradadb();
        return $entradadb->entrada_data($id);
    }

    public function quantprod(){
    $rs = $this->db->count_all('produtos');
    return $rs;
    }

    public function quantcateg(){
        $rs = $this->db->count_all('categoria');
        return $rs;
    }

    public function quantcalendario(){
        $rs = $this->db->count_all('calendar');
        return $rs;
    }

    public function quantmens(){
        $rs = $this->db->count_all('contato');
        return $rs;
    }

    public function entrada(){
        $entrada= new Entradadb();
        return $entrada->entrada();
    }

    public function saida(){
        $saida= new Entradadb();
        return $saida->saida();
    }

}
