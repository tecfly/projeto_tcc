<?php
include_once APPPATH. 'libraries/Cadastro.php';
include_once APPPATH. 'libraries/component/Tabela.php';

class CadastroModel extends CI_Model{
    
    public function gera_tabela(){
        $html = '';
        $Cadastro = new Cadastro();
        $data = $Cadastro->lista_usuario();
    
        $label = ['Nº', 'Nome', 'Sobrenome', 'Email' ];
        $table = new Tabela ($data, $label);
        $table->addHeaderClass('indigo darken-4 white-text text-center');
        $table->addBodyClass('text-center border');
        $table->useHover();
        $table->smallRow();
        $table->useActionButton4();
        return $table->getHTML();
        }
    
    public function guarda_usuario(){
        if(sizeof($_POST) == 0) return;
    
        $this->form_validation->set_rules('nome', 'Nome do usuário', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('sobrenome', 'Sobrenome do usuário', 'trim|required|min_length[5]|max_length[50]');
        $this->form_validation->set_rules('email', 'Email do usuário', 'trim|required|valid_email');
        $this->form_validation->set_rules('senha', 'Senha do usuário', 'trim|required|min_length[6]|max_length[100]');
    
        if($this->form_validation->run()){
            $data = $this->input->post();
            $Cadastro = new Cadastro();
            $Cadastro->criando_usuario($data);
         
            redirect( base_url('index.php/adm/usuario?cadastrado=1'));   
            }
        }
    
    public function edita_usuario($id){
        if(sizeof($_POST) == 0) return;
    
        $data = $this->input->post();
        $Cadastro = new Cadastro();
        $Cadastro->edita_usuario($data, $id);
        redirect('adm/lista_usuario');
    }
    
    public function read($id){
        $Cadastro = new Cadastro();
        return $Cadastro->user_data($id);
    }

    public function deletar($id){
        $Cadastro = new Cadastro();
        $Cadastro->delete($id);
    }

}
