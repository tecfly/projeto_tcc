<?php
include_once APPPATH. 'libraries/Login.php';

class Login_model extends CI_Model {

    public function login_validation(){
        if(sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required|min_length[6]');
        $data['invalido']= "<center>Email ou Senha inválido(a).</center>";

        if($this->form_validation->run()){
            $email = $this->input->post('email');
            $senha = $this->input->post('senha');
            $login = new Login();

            if($login->can_login($email, $senha)){
                $session_data = array(
                    'email' => $email
                );
                $this->session->set_userdata($session_data);
                redirect(base_url('index.php/adm/index'));
            }
        }
    }
}
