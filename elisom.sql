-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 10-Dez-2019 às 19:59
-- Versão do servidor: 10.3.18-MariaDB-0+deb10u1
-- PHP Version: 7.3.11-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gu1800361`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `calendar`
--

CREATE TABLE `calendar` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `content` text NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `calendar`
--

INSERT INTO `calendar` (`id`, `date`, `content`, `status`) VALUES
(2, '2019-12-05 03:00:00', 'Reuniao', 0),
(3, '2020-01-10 03:00:00', 'Sprint', 0),
(4, '2019-12-06 03:00:00', 'Teste 3', 0),
(5, '2019-12-10 03:00:00', 'Teste 4', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `categoria` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`id`, `categoria`, `foto`) VALUES
(1, 'Pneus', 'f4c0993489402c91d609915f0bdbed53.jpg'),
(2, 'Aromatizador', '883a0b7fad763b4ad1a3a2b354620b3a.jpg'),
(4, 'Lâmpadas', 'fb0cbccc7dff9e1bc960e76f79bf0345.png'),
(5, 'Óleos', 'bbf2103971a8c5e2b3c320d69c27052d.jpg'),
(6, 'Rodas', 'fa2381f5c0a2dedcc06ffcf16896210e.jpg'),
(7, 'Paralamas', '0de66df4c823d8c4103df740d8e2ff01.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(11) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `email` varchar(80) NOT NULL,
  `telefone` varchar(30) NOT NULL,
  `mensagem` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `nome`, `email`, `telefone`, `mensagem`, `created_at`) VALUES
(1, 'Yuri dos santos', 'yurisousantos@hotmail.com', '956179398', 'Muito bom o atendimento de seus funcionários!', '2019-12-05 19:06:42'),
(2, 'Wallassy Patrick', 'wallassy_k09@hotmail.com', '970780847', 'Adorei os produtos da loja :)', '2019-12-05 19:07:25'),
(4, 'Luana Guedes', 'luaguedes@gmail.com', '40028922', 'Eu gostaria de encomendar óleo.', '2019-12-05 19:08:47'),
(5, 'Gabriel Blesa', 'blesa90@outlook.com', '(11)942836133', 'OIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII', '2019-12-10 18:29:36'),
(6, 'Gabriel Blesa', 'blesa90@outlook.com', '(11)942836133', 'OIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII', '2019-12-10 18:29:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `movimentos`
--

CREATE TABLE `movimentos` (
  `id_movimento` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `quant` int(11) NOT NULL,
  `data_create` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `movimentos`
--

INSERT INTO `movimentos` (`id_movimento`, `id_produto`, `quant`, `data_create`) VALUES
(30, 5, 2, '2019-12-05 19:36:58'),
(31, 7, -5, '2019-12-05 19:37:19'),
(32, 5, 5, '2019-12-05 19:38:30'),
(33, 9, -2, '2019-12-05 19:38:39'),
(34, 5, 2, '2019-12-05 20:18:10'),
(35, 5, -5, '2019-12-05 20:18:25'),
(36, 10, 30, '2019-12-09 21:33:58'),
(37, 10, -10, '2019-12-09 21:34:10'),
(38, 20, 10, '2019-12-09 21:41:13'),
(39, 20, 20, '2019-12-09 21:41:26'),
(40, 5, 2, '2019-12-10 18:19:12'),
(41, 6, -10, '2019-12-10 18:19:21'),
(42, 5, 50, '2019-12-10 20:41:53'),
(43, 8, -2, '2019-12-10 20:42:02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `categ` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `preco` varchar(100) NOT NULL,
  `quantidade` int(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `promocao` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `nome`, `categ`, `descricao`, `preco`, `quantidade`, `foto`, `promocao`) VALUES
(5, 'Dois Pneus Moto Pirelli', '1', '140-70r17 66h 110-70r17 54h Diablo Rosso 2', '612,00', 64, '5a8fa2fee9d934d1c88c2e3c9b6565c9.jpg', 1),
(6, 'Jogo de 4 Pneus Michelin Aro 15', '1', 'Primacy 3 GNRX 195-65R15 91H TL - Original Cobalt', '1451,99', 2, '7b67dc459dba27587a4be035f7480573.jpg', 1),
(7, 'Kit 2 pneus Michelin', '1', '175-65R14 82T G-GRIP GO', '522,00', 5, '6af6705e046fee0b4b34bc1f19a864e1.jpg', 1),
(8, 'Kit 4 Pneus Pirelli Aro 14', '1', '175-65R14 P400 EVO', '779,00', 10, '6e51e3f0ac9ca5bff2d996fe00670f6c.jpg', 1),
(9, 'Kit 4 Pneus Michelin Aro15', '1', '185-60R15 88H XL TL Primacy 4 MI', '1289,90', 10, '3ffc6a7b4fba918a191a638bd90f9e98.jpg', 0),
(10, 'Pneu 225-40R18', '1', '92W Sport+2 Extra Load Xbri', '281,90', 40, '694759d2e1233a12fbcac99303e2b090.jpg', 1),
(11, 'Pneu Aro 14 Pirelli', '1', '175-65R14 82H - P400 EVO', '219,90', 22, 'b6eb4fa682dbf6c9c7de9a864ecb6745.jpg', 1),
(12, 'Pneu Aro 16  Michelin', '1', 'Primacy 4 205_55 R16 - 91V Primacy 4', '313,50', 22, '34db8beb6bcb55746dd38721343d35be.jpg', 1),
(13, 'Pneu Aro 20', '1', '295-35R20 Continental', '2010,00', 30, 'd9fa1c1c444a6ce3502438833867fb68.jpg', 0),
(14, 'Pneu de Moto Aro 17 Pirelli', '1', 'Traseiro 110-90 - 60P MT60 Trail', '269,90', 22, '12d57fcb6219688021eee09b34d1c237.jpg', 0),
(15, 'Pneu Firestone Aro 15', '1', 'Destination MT 23 31X10.5R15 109Q LT', '549,90', 22, '2fcb49b62aae0a3a507bd3013b8bdbf2.jpg', 0),
(16, 'Pneu Michelin Aro 16', '1', 'LTX Force 265-70R16 112T', '617,00', 22, '1c5784a01f9c38301855bcebdc7465e6.jpg', 0),
(17, 'f1 master', '5', 'protection 20w50', '18,00', 30, '88af975c20d33eed97362c8a470ad19d.png', 0),
(18, 'Lubrificante mineral lubrax', '5', 'lubrax', '16,00', 30, '1c72fbc0d9b95ebb542cbab9bed53768.png', 1),
(19, 'Oleo lubrificante', '5', '10w40 semi sintetico shell helix', '32,00', 22, 'f20fab8a24add92f1d562aa330eadf82.png', 0),
(20, 'Mobil super', '5', '20w40 mineral SL', '19,00', 52, '95aca83d58b6f7556d5c53c6aeb5d36e.png', 0),
(21, 'Pneu aro 20', '1', 'testetetsfsfhdjgnlkhj', '100,00', 20, '96ee553638fdf03f9213fdfe23e2b392.jpg', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `sobrenome` varchar(30) NOT NULL,
  `email` varchar(150) NOT NULL,
  `senha` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `sobrenome`, `email`, `senha`) VALUES
(10, 'Wellington', 'Pinheiro', 'wellingtonpinheiro@gmail.com', 'pinheirovigini2019'),
(11, 'Higor', 'Santos', 'higors@gmail.com', 'elisomiguinho'),
(12, 'Vitoria', 'De Lima', 'vitoria11212628@gmail.com', 'vitoria');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `calendar`
--
ALTER TABLE `calendar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movimentos`
--
ALTER TABLE `movimentos`
  ADD PRIMARY KEY (`id_movimento`),
  ADD KEY `fk_produtos` (`id_produto`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `calendar`
--
ALTER TABLE `calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `movimentos`
--
ALTER TABLE `movimentos`
  MODIFY `id_movimento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `movimentos`
--
ALTER TABLE `movimentos`
  ADD CONSTRAINT `fk_produtos` FOREIGN KEY (`id_produto`) REFERENCES `produtos` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
